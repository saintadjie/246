@extends('layouts.outer')
@push('script-header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')

<div class="section-body">
    <h2 class="section-title">Publik</h2>
    <p class="section-lead">Halaman ini dibuat untuk publik.</p>

    <div class="row">
              
        <div class="col-12 col-sm-12 col-lg-12">
        	<div class="card">
            	<div class="card-body">
                    <h4>Custom Search :</h4>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tanggal_awal">Tanggal Awal</label>
                            <input type="text" class="form-control datepicker" id="tanggal_awal" name="tanggal_awal">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tanggal_akhir">Tanggal Akhir</label>
                            <input type="text" class="form-control datepicker" id="tanggal_akhir" name="tanggal_akhir">
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <button id="btn_cari" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Cari</button>
                        </div>
                    </div>
                    <hr/>
                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white; width: 30%;">Satker</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Total Kuota</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Sisa Kuota</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script-footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/public/mpaspor/kuota_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_main            = "{{url('/administratif/mpaspor/kuota')}}"
    </script>

    <script type="text/javascript">
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],

            destroy: true,
            processing: true,
            serverSide: true,
            order: [[1, 'asc']],
            "ajax": {
                "url": "{{ route ('pullData.publicmpasporkuota') }}",
                "type": "GET",
                "data": {
                    type : 'pullData',
                },
            },
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "nama",
                    "sClass": "text-center",
                },
                {
                    "data": "tanggal_kuota",
                    "sClass": "text-center",
                },
                {
                    "data": "total_kuota",
                    "sClass": "text-center",
                },
                {
                    "data": "sisa_kuota",
                    "sClass": "text-center",
                },
            ],
        });


    </script>

    <script>
        $('#btn_cari').click(function(){
            var from = $("#tanggal_awal").val();
            var to = $("#tanggal_akhir").val();

            
            if ($('#tanggal_awal').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Tanggal Awal tidak boleh kosong", "error" )
                return false
            }
            else if ($('#tanggal_akhir').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Tanggal Akhir tidak boleh kosong", "error" )
                return false
            }
            else if(Date.parse(from) > Date.parse(to)){
               Swal.fire( "Kesalahan", "Kolom Tanggal Akhir tidak boleh lebih awal dari Kolom Tanggal Awal", "error" )
                return false
            }

            
            var table_form = $('#table_form').DataTable({
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
                destroy: true,
                processing: true,
                serverSide: true,
                order: [[1, 'asc']],
                "ajax": {
                    "url": "{{ route ('pullData.publicmpasporkuota') }}",
                    "type": "GET",
                    "data": {
                        type : 'pullData',
                        tanggal_awal    : $('#tanggal_awal').val(),
                        tanggal_akhir    : $('#tanggal_akhir').val(),
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_kuota",
                        "sClass": "text-center",
                    },
                    {
                        "data": "total_kuota",
                        "sClass": "text-center",
                    },
                    {
                        "data": "sisa_kuota",
                        "sClass": "text-center",
                    },
                ],
            });

        });
    </script>

@endpush
@endsection
