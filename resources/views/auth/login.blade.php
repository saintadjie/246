@extends('layouts.out')

@section('content')
    
    <div class="login-root">
        <div class="box-root flex-flex flex-direction--column" style="min-height: 100vh;flex-grow: 1;">
            <div class="loginbackground box-background--white padding-top--64">
                <div class="loginbackground-gridContainer">
                    <div class="box-root flex-flex" style="grid-area: top / start / 8 / end;">
                        <div class="box-root" style="background-image: linear-gradient(white 0%, rgb(247, 250, 252) 33%); flex-grow: 1;"> </div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 4 / 2 / auto / 5;">
                        <div class="box-root box-divider--light-all-2 animationLeftRight tans3s" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 6 / start / auto / 2;">
                        <div class="box-root box-background--blue800 animationLeftRight tans3s" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 7 / start / auto / 4;">
                        <div class="box-root box-background--blue animationLeftRight" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 8 / 4 / auto / 6;">
                        <div class="box-root box-background--gray100 animationLeftRight tans3s" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 2 / 15 / auto / end;">
                        <div class="box-root box-background--cyan200 animationRightLeft tans4s" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 3 / 14 / auto / end;">
                        <div class="box-root box-background--blue animationRightLeft" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 4 / 17 / auto / 20;">
                        <div class="box-root box-background--gray100 animationRightLeft tans4s" style="flex-grow: 1;"></div>
                    </div>
                    <div class="box-root flex-flex" style="grid-area: 5 / 14 / auto / 17;">
                        <div class="box-root box-divider--light-all-2 animationRightLeft tans3s" style="flex-grow: 1;"></div>
                    </div>
                </div>
            </div>

            <div class="box-root padding-top--24 flex-flex flex-direction--column" style="flex-grow: 1; z-index: 9;">
                <div class="box-root padding-top--48 padding-bottom--80 flex-flex flex-justifyContent--center">
                    <h1><a href="http://10.0.22.246/" rel="dofollow">IMIGRASI</a></h1>
                </div>
                <div class="formbg-outer">
                    <div class="formbg">
                        <div class="formbg-inner padding-horizontal--48">
                            <span class="padding-bottom--15">Sign in to your account</span>
                            <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="" id="stripe-login">
                                @csrf
                                <div class="row">
                                    <div class="input-field col s12">
                                        @if ($errors->has('username') || $errors->has('password'))
                                            <div class="alert alert-danger">
                                                <center>
                                                    <strong>{{ $errors->first('username') ?: $errors->first('password')}}</strong>
                                                </center>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Username</label>
                                    <input id="login" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="login" value="{{ old('username') }}"  tabindex="1" required autofocus>
                                    <div class="invalid-feedback">
                                        Harap isi username anda
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">Password</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  tabindex="2" required>
                                    <div class="invalid-feedback">
                                        Harap isi password anda
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Continue
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="footer-link padding-top--48">
                        <span>Don't have an account? <a href="#">Sign up</a></span>
                        <div class="listing padding-top--24 padding-bottom--24 flex-flex center-center">
                            <span><a href="#">10.0.22.246</a></span>
                            <span><a href="#">© Imigrasi</a></span>
                            <span><a href="#">2022</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection