
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>IMIGRASI</title>
    <link rel="icon" type="image/png" href="{{url('passport.png')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    @stack('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/all.min.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/sweetalert2/sweetalert2.min.css')}}" />
    <style type="text/css">
        
    </style>
</head>

<body>
    <div class="modal-content" id="modal_password_profile">
        <div class="modal-body">
            <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>
                    <input id="password_profile" type="password" class="form-control" name="password_profile">
                </div>
            </div>
            <div class="form-group">
                <label>Konfirmasi Password</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>
                    <input id="konfirmasi_password_profile" type="password" class="form-control" name="konfirmasi_password_profile">
                </div>
            </div>
        </div>
        <div class="modal-footer bg-whitesmoke br">
            <a class="btn btn-primary btn-shadow text-white mr-1" id="btn_simpan_password_profile">Ubah Password</a>
        </div>
    </div>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <ul class="navbar-nav mr-3 mr-auto">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                </ul>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            @if(Auth::user()->photo != null)
                                <img alt="image" src="{{url(Auth::user()->photo)}}" class="rounded-circle mr-1">
                            @else
                                <img alt="image" src="{{url('images\user.png')}}" class="rounded-circle mr-1">
                            @endif
                            
                            <div class="d-sm-none d-lg-inline-block">Hi, {{Auth::User()->nama}}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-title">
                                @if(Auth::user()->last_login_time != null)
                                    Login sejak {{ auth()->user()->last_login_time->diffForHumans() }}
                                @else
                                    {{$waktu}}
                                @endif
                            </div>
                            <a href="#" id="btn_password_profile" class="dropdown-item has-icon">
                                <i class="fas fa-lock"></i> Ganti Password
                            </a>
                            <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>

            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="{{url('/home')}}">IMIGRASI</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="{{url('/home')}}">IMI</a>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li><a class="nav-link" href="{{url('/home')}}"><i class="fas fa-fire" style="color: red"></i> <span>Dashboard</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/user')}}"><i class="fas fa-user" style="color: rebeccapurple;"></i> <span>Pengguna</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-passport" style="color: rebeccapurple;"></i> <span>APAPO</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/apapo')}}"><i class="fas fa-star fa-spin" style="color: rebeccapurple;"></i> <span>VIP</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-user-friends" style="color: rebeccapurple;"></i> <span>Dukcapil</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/dukcapil')}}"><i class="fas fa-user-friends" style="color: rebeccapurple;"></i> <span>Dukcapil</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-user-secret" style="color: rebeccapurple;"></i> <span>LHI</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/lhi')}}"><i class="fas fa-newspaper" style="color: rebeccapurple;"></i> <span>Report</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/lhi/index_error')}}"><i class="fas fa-newspaper" style="color: rebeccapurple;"></i> <span>Report Error</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/kirka')}}"><i class="fas fa-newspaper" style="color: rebeccapurple;"></i> <span>Kirka</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-passport" style="color: rebeccapurple;"></i> <span>M-Paspor</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor')}}"><i class="fas fa-fire fa-spin" style="color: Red;"></i> <span>API</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/simponi')}}"><i class="fas fa-donate" style="color: rebeccapurple;"></i> <span>Check Simponi</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/generate')}}"><i class="fas fa-wind" style="color: rebeccapurple;"></i> <span>Generate Kode Billing</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/status_permohonan')}}"><i class="fas fa-syringe" style="color: rebeccapurple;"></i> <span>Update Status Master</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/error_log_synx')}}"><i class="fas fa-skull-crossbones" style="color: rebeccapurple;"></i> <span>Error Log Synx</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/kuota')}}"><i class="fas fa-calendar" style="color: rebeccapurple;"></i> <span>Kuota</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/kuota_terpakai')}}"><i class="fas fa-calendar-check" style="color: rebeccapurple;"></i> <span>Kuota Terpakai</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/permohonan')}}"><i class="fas fa-window-restore" style="color: rebeccapurple;"></i> <span>List Permohonan</span></a></li>
                                <li><a class="nav-link" href="{{url('/administratif/mpaspor/satuan_kerja')}}"><i class="fas fa-sitemap" style="color: rebeccapurple;"></i> <span>Satuan Kerja</span></a></li>
                            </ul>
                        </li>
                        <li><a class="nav-link" href="{{url('/administratif/simponi')}}"><i class="fas fa-money-bill-wave" style="color: rebeccapurple;"></i> <span>Simponi</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-sync" style="color: rebeccapurple;"></i> <span>Synxchro</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/synxchro')}}"><i class="fas fa-sync" style="color: rebeccapurple;"></i> <span>Node M-Paspor</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-skull" style="color: rebeccapurple;"></i> <span>Explicit Contents</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{url('/administratif/dbdpripassword')}}"><i class="fas fa-database" style="color: rebeccapurple;"></i> <span>DB DPRI</span></a></li>
                            </ul>
                        </li>

                        <li><a class="nav-link" href="{{url('/administratif/logs')}}"><i class="fas fa-clipboard-list" style="color: rebeccapurple;"></i> <span>Log Aktifitas</span></a></li>
                        
                        <li><a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt" style="color: red"></i> <span>Logout</span></a></li>
                    </ul>
                </aside>
            </div>

            <!-- Main Content -->
            <a id="backtotop"></a>
            <div class="main-content">
                <section class="section">
          
                    @yield('content')

                </section>
             </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2022 <div class="bullet"></div> Design For <a href="#">IMIGRASI</a>
                </div>
                <div class="footer-right">
          
                </div>
            </footer>
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="{{url('out/js/jquery.min.js')}}"></script>
    <script src="{{url('out/js/popper.js')}}"></script>
    <script src="{{url('out/js/tooltip.js')}}"></script>
    <script src="{{url('out/js/bootstrap.min.js')}}"></script>
    <script src="{{url('out/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{url('out/js/moment.min.js')}}"></script>
    <script src="{{url('out/js/stisla.js')}}"></script>



    <!-- Template JS File -->
    <script src="{{url('out/js/scripts.js')}}"></script>
    <script src="{{url('out/js/custom.js')}}"></script>
    <script src="{{url('out/css/sweetalert2/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Requested-With': 'XMLHttpRequest',
            }
        })
    </script>
    <script type="text/javascript">
        var backtotop = $('#backtotop');

        $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
            backtotop.addClass('show');
          } else {
            backtotop.removeClass('show');
          }
        });

        backtotop.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({scrollTop:0}, '300');
        });


    </script>
    <script type="text/javascript">
        var url_logout          = "{{url('logout')}}";
        var file_name_loader    = ['giphy-a.gif', 'giphy-b.gif', 'giphy-c.gif', 'giphy-d.gif', 'giphy-e.gif', 'giphy-f.gif', 'giphy-g.gif', 'giphy-h.gif', 'giphy-i.gif', 'giphy-j.gif'];
    </script>
    <script type="text/javascript">
        $("#btn_password_profile").fireModal({
            title: 'UBAH PASSWORD',
            body: $("#modal_password_profile"),
            center: true,
            autoFocus: false,
        });

        $('#btn_simpan_password_profile').click(function(){
            if ($('#password_profile').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Password tidak boleh kosong", "error" )
                return false
            }
            else if ($('#konfirmasi_password_profile').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Konfirmasi Password tidak boleh kosong", "error" )
                return false
            } 
            else if ($('#password_profile').val() != $('#konfirmasi_password_profile').val()) {
                Swal.fire("Kesalahan", "Password dan Konfirmasi Password tidak cocok", "error");
                return false
            }

            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Password anda akan diubah?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3051d3",
                cancelButtonColor: '#d33',
                cancelButtonText: "Batal"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "PUT",
                        url: "{{url('api/v1/administratif/user/edit_password_profile')}}",
                        data: {
                            password        : $("#password_profile").val(),
                        },
                        success: function(data) {
                            if(data.status == 'OK'){
                                Swal.fire({
                                    icon: "success",
                                    title: "Data telah diubah",
                                    timer: 3000,
                                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__zoomOut'
                                    },
                                    backdrop: `
                                        rgba(0,0,123,0.4)
                                        url("../images/nyan-cat.gif")
                                        left top
                                        no-repeat
                                      `,
                                    timerProgressBar: true,
                                    onBeforeOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                          const content = Swal.getContent()
                                          if (content) {
                                            const b = content.querySelector('b')
                                            if (b) {
                                              b.textContent = Swal.getTimerLeft()
                                            }
                                          }
                                        }, 100)
                                    },
                                    onClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then(function(){
                                    location.href = url_logout
                                });
                            } else {
                                Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                            }   
                        }
                    });
                }
            })
            
        });
    </script>
    @stack('script-footer')
</body>
</html>