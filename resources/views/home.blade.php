@extends('layouts.in')
@section('content')
<div class="section-header">
    <h1>Dashboard</h1>
</div>
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                Selamat datang, {{Auth::User()->nama}}!
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <h4>Dashboard Hari Ini</h4>    
            </div>

            <div class="card-body">
                <div class="summary">
                
                    <div class="summary-item">
                        <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/user.jpg')}}" alt="user">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countuser}}</div>
                                    <div class="media-title"><a href="#">Pengguna</a></div>
                                    <div class="text-muted text-small">Daftar pengguna terdaftar</div>
                                </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/lhi.jpg')}}" alt="lhi">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$counttotallhi[0]->total}}</div>
                                    <div class="media-title"><a href="#">Total LHI</a></div>
                                    <div class="text-muted text-small">Laporan Harian Intelijen</div>
                                </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/lhiharian.jpg')}}" alt="lhi">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countharianlhi[0]->total}}</div>
                                    <div class="media-title"><a href="#">LHI</a></div>
                                    <div class="text-muted text-small">Laporan Harian Intelijen tanggal {{$today}}</div>
                                </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/mpaspor.jpg')}}" alt="mpaspor">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countpermohonanmpasportoday[0]->total}}</div>
                                    <div class="media-title"><a href="#">Permohonan M-Paspor</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <h4>Dashboard M-Paspor Hari Ini</h4>    
            </div>

            <div class="card-body">
                <div class="summary">
                
                    <div class="summary-item">
                        <ul class="list-unstyled list-unstyled-border">

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/mpasporuncompiled.png')}}" alt="mpaspor">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countnotcompiledmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Permohonan M-Paspor Not Yet Compiled</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/mpasporunsync.jpg')}}" alt="mpaspor">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countnotsyncmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Permohonan M-Paspor Unsynced (0)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/dokumenunsync.png')}}" alt="dokumen">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countnotsyncdokumen[0]->total}}</div>
                                    <div class="media-title"><a href="#">Dokumen M-Paspor Unsynced (0)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/mpasporfailed.jpg')}}" alt="dokumen">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countfailedmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Permohonan M-Paspor Status Failed (3)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/documentfailed.jpg')}}" alt="dokumen">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countdocumentfailedmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Dokumen M-Paspor Status Failed (3)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/mpasporhold.jpg')}}" alt="dokumen">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countholdmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Permohonan M-Paspor Status Hold (9)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>

                            <li class="media">
                                <a href="#">
                                    <img class="mr-3 rounded" width="60" src="{{url('/images/documenthold.jpg')}}" alt="dokumen">
                                </a>
                                <div class="media-body">
                                    <div class="media-right">{{$countdocumentholdmpaspor[0]->total}}</div>
                                    <div class="media-title"><a href="#">Dokumen M-Paspor Status Hold (9)</a></div>
                                    <div class="text-muted text-small">Tanggal {{$today}}</div>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>

<div class="container">
    
</div>
@endsection
