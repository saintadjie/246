<?php

return [
    'apapo' => [
        'driver'         => 'oracle',
        'host'           => env('DB_HOST_ORACLE', '10.160.110.17'),
        'port'           => env('DB_PORT_ORACLE', '1521'),
        'database'       => env('DB_DATABASE_ORACLE', 'IMGMOBIL'),
        'service_name'   => env('DB_SERVICE_NAME_ORACLE','imgmobile'),
        'username'       => env('DB_USERNAME_ORACLE', 'IMGWNI'),
        'password'       => env('DB_PASSWORD_ORACLE', 'P@ssw0rd'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '12g'),
    ],

    'LHI' => [
        'driver'         => 'oracle',
        'host'           => env('DB_HOST_ORACLE', 'imiexa-scan.imigrasi.go.id'),
        'port'           => env('DB_PORT_ORACLE', '1521'),
        'database'       => env('DB_DATABASE_ORACLE', 'IMGMOBIL'),
        'service_name'   => env('DB_SERVICE_NAME_ORACLE','imgmobile'),
        'username'       => env('DB_USERNAME_ORACLE', 'IMGPENGAWASAN'),
        'password'       => env('DB_PASSWORD_ORACLE', 'P@ssw0rd'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '12g'),
    ],



    
];