<?php

namespace App\Console\Commands;
use Log;
use Auth;
use DB;
use Carbon\Carbon;

use Illuminate\Console\Command;

class GenerateCron extends Command
{

    protected $db;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->db = DB::connection('mpaspor');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $date               = Carbon::now()->addDays(1)->startOfDay();

        $time               = Carbon::now()->subMinute(10);

        $kode_permohonan    = ($this->db->select("SELECT a.permohonan_id FROM po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan >= '$date' and b.submit_date <= '$time' and b.status in (2,3,4)  and (a.kode_permohonan is not null AND a.kode_permohonan != '-') order by b.submit_date"));

        $length     = count($kode_permohonan);
        $generate   = '';
        for ($i = 0; $i < $length; $i++) {
            $generate = $generate . $kode_permohonan[$i]->permohonan_id . ',';
            if ($i == $length-1) {
              $generate = $generate . $kode_permohonan[$i]->permohonan_id;
            }
        }
        
        $update_master  = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '4' WHERE id IN ($generate)"));

        if ($update_master){

            $url                = "http://10.20.66.13:8089/api/simponi/bridge/trigger-generate-billing";
            $rstoken            = $this->db->select(DB::raw("SELECT a.uuid_token FROM po_uma.uma_jwt_token a join po_uma.uma_tbl_users b on a.user_id = b.id WHERE a.is_expired = '0' and b.role_id = '2' ORDER BY a.expired_at desc LIMIT 1"));

            $curl   = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
               "Accept: application/json",
               "Authorization: Bearer ". $rstoken[0]->uuid_token,
               "Content-Type: application/json",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $hasil = curl_exec($curl);
            curl_close($curl);
            $rs = json_decode($hasil);

            if ($rs->status == 'true'){
                
                activity()
                ->withProperties(['username' => 'SYSTEM'])
                ->log('System berhasil merefresh Kode Billing secara otomatis');

            } else {
                
                activity()
                ->withProperties(['username' => 'SYSTEM'])
                ->log('System gagal merefresh Kode Billing secara otomatis');
            }

        } else {

            activity()
                ->withProperties(['username' => 'SYSTEM'])
                ->log('System gagal merefresh Kode Billing secara otomatis');

        }

        
    }
}
