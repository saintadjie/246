<?php

namespace App\Console\Commands;
use Log;
use Auth;
use DB;
use Carbon\Carbon;

use Illuminate\Console\Command;

class UpdateStatusCron extends Command
{

    protected $db;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatestatus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->db = DB::connection('mpaspor');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date       = Carbon::now()->startOfDay();
        $now        = Carbon::now();

        $rs         = $this->db->select("SELECT b.id as id_master FROM po_mpp.mpp_tbl_detail_booking_pemohon a JOIN po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id WHERE a.status = 3 AND a.is_delete in (0) AND b.tanggal_pengajuan >= '$date' AND a.kadalauarsa_kode_billing <= '$now' AND b.master_document_link is not NULL AND b.status in (2)");

        $length     = count($rs);
        $generate   = '';
        for ($i = 0; $i < $length; $i++) {
            $generate = $generate . $rs[$i]->id_master . ',';
            if ($i == $length-1) {
              $generate = $generate . $rs[$i]->id_master;
            }
        }

        $id_master  = $generate;
        
        $result             = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '3' WHERE id IN ($id_master)"));
     
        if ($result){

            activity()
            ->withProperties(['username' => 'SYSTEM'])
            ->log('System berhasil mengupdate status master secara otomatis');

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => 'SYSTEM'])
            ->log('System gagal mengupdate status master secara otomatis');

            return response()->json(['status' => 'ERROR']);

        }
    }
}
