<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->dblhi = DB::connection('LHI');
        $this->mpaspor = DB::connection('mpaspor');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    protected $db;

   

    public function index()
    {
        $today                          = Carbon::now()->format('Y-m-d');
        $todaylhi                       = Carbon::now()->startOfDay();
        $counttotallhi                  = $this->dblhi->select(DB::raw("SELECT COUNT(*) AS total FROM IMGPENGAWASAN.REPORT_INTEL"));
        $countharianlhi                 = $this->dblhi->select(DB::raw("SELECT COUNT(*) AS total FROM IMGPENGAWASAN.REPORT_INTEL WHERE reported_on > (TIMESTAMP '$todaylhi')"));
        $countnotcompiledmpaspor        = $this->mpaspor->select(DB::raw("SELECT count(*) AS total from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where b.tanggal_pengajuan = '$today' and a.status in (3) and a.ready_to_sync in (0)"));
        $countnotsyncmpaspor            = $this->mpaspor->select(DB::raw("SELECT COUNT(*) AS total FROM po_boi.tx_paspor WHERE po_boi.tx_paspor.tanggal_permohonan ='$today' AND po_boi.tx_paspor.sts_sync = '0'"));
        $countnotsyncdokumen            = $this->mpaspor->select(DB::raw("SELECT count(tkd.id) AS total FROM po_boi.tx_paspor tp JOIN po_boi.tx_kelengkapan_dokumen tkd ON tp.kode_permohonan = tkd.kode_permohonan WHERE tp.tanggal_permohonan = '$today' AND tkd.sts_sync = '0'"));
        $countfailedmpaspor             = $this->mpaspor->select(DB::raw("SELECT COUNT(*) AS total FROM po_boi.tx_paspor WHERE po_boi.tx_paspor.tanggal_permohonan ='$today' AND po_boi.tx_paspor.sts_sync = '3'"));
        $countdocumentfailedmpaspor     = $this->mpaspor->select(DB::raw("SELECT COUNT(*) AS total FROM po_boi.tx_paspor WHERE po_boi.tx_paspor.tanggal_permohonan ='$today' AND po_boi.tx_paspor.sts_sync = '3'"));
        $countholdmpaspor               = $this->mpaspor->select(DB::raw("SELECT COUNT(*) AS total FROM po_boi.tx_paspor WHERE po_boi.tx_paspor.tanggal_permohonan ='$today' AND po_boi.tx_paspor.sts_sync = '9'"));
        $countdocumentholdmpaspor       = $this->mpaspor->select(DB::raw("SELECT COUNT(*) AS total FROM po_boi.tx_paspor WHERE po_boi.tx_paspor.tanggal_permohonan ='$today' AND po_boi.tx_paspor.sts_sync = '9'"));
        $countuser                      = User::count();
        $countpermohonanmpasportoday    = $this->mpaspor->select(DB::raw("SELECT COUNT(a.kode_permohonan) AS total FROM po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id and b.tanggal_pengajuan = '$today' WHERE a.ready_to_sync in (0,1) and a.status in (3)"));

        return view('home', ['today'=> $today, 'countuser'=> $countuser, 'counttotallhi'=> $counttotallhi, 'countharianlhi'=> $countharianlhi, 'countnotcompiledmpaspor'=> $countnotcompiledmpaspor, 'countnotsyncmpaspor'=> $countnotsyncmpaspor, 'countnotsyncdokumen'=> $countnotsyncdokumen, 'countfailedmpaspor'=> $countfailedmpaspor, 'countdocumentfailedmpaspor'=> $countdocumentfailedmpaspor, 'countholdmpaspor'=> $countholdmpaspor, 'countdocumentholdmpaspor'=> $countdocumentholdmpaspor, 'countpermohonanmpasportoday'=> $countpermohonanmpasportoday]);
    }

    public function cek() {
        // get logged-in user
        $user = auth()->user();
        $user->givePermissionTo('Melihat daftar pengguna');

        // get all inherited permissions for that user
        $permissions = $user->getAllPermissions();

        return response()->json([
            'b' => $permissions
        ], 200);
    }
}
