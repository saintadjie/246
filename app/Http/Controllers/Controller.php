<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function randId() {
		/*random Id*/
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEF".date("d").date("m").date("y")."GHIJKLMNOPQRSTUWXYZ0123456789";
		$id = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$id[] = $alphabet[$n];
		}

		for ($a = 0; $a < 4; $a++) {
			$n = rand(0, $alphaLength);
			$id2[] = $alphabet[$n];
		}

		for ($b = 0; $b < 4; $b++) {
			$n = rand(0, $alphaLength);
			$id3[] = $alphabet[$n];
		}

		for ($c = 0; $c < 4; $c++) {
			$n = rand(0, $alphaLength);
			$id4[] = $alphabet[$n];
		}

		for ($c = 0; $c < 12; $c++) {
			$n = rand(0, $alphaLength);
			$id5[] = $alphabet[$n];
		}
		return $randId=strtoupper(implode($id)."-".implode($id2)."-".implode($id3)."-".implode($id4)."-".implode($id5));
	}
}
