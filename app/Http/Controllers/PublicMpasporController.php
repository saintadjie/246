<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;

class PublicMpasporController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function get_permohonanPm(Request $request)
    {

        $kpnkb      = $request->kpnkb;

        $rs         = $this->db->select(DB::raw("SELECT a.id, a.kode_permohonan, a.nik, a.nama, d.nama as satker, CASE WHEN a.status = 4 THEN 'Kadaluarsa' WHEN a.status = 3 THEN 'Terbayar' WHEN a.status = 2 THEN 'Menunggu Pembayaran' WHEN a.status = 1 THEN 'Draft' ELSE 'Undefined' END as status_detail, CASE WHEN b.status = 4 THEN 'Kadaluarsa' WHEN b.status = 3 THEN 'Terbayar' WHEN b.status = 2 THEN 'Menunggu Pembayaran' WHEN b.status = 1 THEN 'Draft' ELSE 'Undefined' END as status_permohonan, a.simponi_billing_code, a.kadalauarsa_kode_billing, b.submit_date as tanggal_submit, b.tanggal_pengajuan as tanggal_kedatangan, c.nama_sesi, b.rescheduled_count, CASE WHEN b.rescheduled_count = 1 THEN 'Tidak' ELSE 'Ya' END AS rescheduled from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on b.id = a.permohonan_id join po_kma.tbl_kuota_persesi c on b.sesi = c.id join po_kma.kma_tbl_kanim d on d.id = b.kanim_id where a.nik = '$kpnkb' or a.kode_permohonan = '$kpnkb' or a.simponi_billing_code = '$kpnkb'"));

        if ($rs){

            foreach ($rs as $key => $value) {
                $data = array_search($value->id, array_column($rs, 'id'));
                
                $id = $rs[$data]->id;
                $value->kode_permohonan = $rs[$data]->kode_permohonan;
                $value->nik = $rs[$data]->nik;
                $value->nama = $rs[$data]->nama;
                $value->satker = $rs[$data]->satker;
                $value->status_detail =$rs[$data]->status_detail;
                $value->status_permohonan = $rs[$data]->status_permohonan;
                $value->simponi_billing_code = $rs[$data]->simponi_billing_code;
                $value->kadalauarsa_kode_billing = $rs[$data]->kadalauarsa_kode_billing;
                $value->tanggal_submit = $rs[$data]->tanggal_submit;
                $value->tanggal_kedatangan = $rs[$data]->tanggal_kedatangan;
                $value->nama_sesi = $rs[$data]->nama_sesi;

            }

            return response()->json(['status' => 'OK', 'rs' => $rs]);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function check_simponiPm(Request $request)
    {

        $url = "http://10.0.22.103:8088/simponi/trx/inquiry";
        $uid = '095f357bb1885755-c3be8f09-9a48819d';
        $kode_billing = $request->kode_billing;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
           "uid: $uid",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);



        $data = '{"method": "inquirybilling_v2", 
                "data": [
                    "202112071635140000000",
                    "121917",
                    "SIMKIM9i4a",
                    "'.$kode_billing.'", 
                    "013",
                    "06",
                    "409272"
                ]}';

        $datampaspor         = $this->db->select(DB::raw("SELECT a.nama from po_mpp.mpp_tbl_detail_booking_pemohon a where a.simponi_billing_code = '$kode_billing'"));

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);


        curl_close($curl);
        $rs = json_decode($hasil);
        $rsstatus = $rs->response;
        
        if ($rsstatus->code == '00'){
            if(empty($datampaspor[0])){
                $result = $rs;
                $rsmessage = $rs->response->message;

                return response()->json(['status' => 'OK', 'result' => $result, 'rsmessage' => $rsmessage, 'datampaspor' => '-']);
            } else {
                $result = $rs;
                $rsmessage = $rs->response->message;

                return response()->json(['status' => 'OK', 'result' => $result, 'rsmessage' => $rsmessage, 'datampaspor' => $datampaspor[0]->nama]);
            }

        } else {
            $rsmessage = $rs->response->message;

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }
    }

    public function check_reschedulePm(Request $request)
    {

        $kode_permohonan_reschedule     = $request->kode_permohonan_reschedule;
        $usernames  = 'Pengunjung Publik dengan IP - '.$_SERVER['REMOTE_ADDR'];

        $rs         = $this->db->select(DB::raw("SELECT a.nik, a.kode_permohonan, a.nama, (select nama from po_kma.kma_tbl_kanim where id = b.before_kanim_id) as KANIM_SEBELUM, (select nama from po_kma.kma_tbl_kanim where id = b.after_kanim_id) as KANIM_SESUDAH, (select nama_sesi from po_kma.tbl_kuota_persesi where id = b.before_session_id) as SESI_SEBELUM, (select nama_sesi from po_kma.tbl_kuota_persesi where id = b.after_session_id) as SESI_SESUDAH, (select tanggal_kuota from po_kma.kma_tbl_kuota_harian where id = c.kuota_id) as TANGGAL_SEBELUM, (select tanggal_kuota from po_kma.kma_tbl_kuota_harian where id = d.kuota_id) as TANGGAL_SESUDAH from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_reschedule_history b on a.permohonan_id = b.passport_application_id join po_kma.tbl_kuota_persesi c on b.before_session_id = c.id join po_kma.tbl_kuota_persesi d on b.after_session_id = d.id where a.kode_permohonan = '$kode_permohonan_reschedule'"));

        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek data reschedule Kode Permohonan '. $kode_permohonan_reschedule);

            return response()->json(['status' => 'OK', 'rs' => $rs]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek data reschedule Kode Permohonan '. $kode_permohonan_reschedule);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function status_emailPm(Request $request)
    {

        $status_email   = $request->status_email;
        $usernames      = 'Pengunjung Publik dengan IP - '.$_SERVER['REMOTE_ADDR'];

        $rs         = $this->db->select(DB::raw("SELECT a.email, a.nomor_telepon, CASE WHEN a.status = 2 THEN 'Tersuspend' WHEN a.status = 1 THEN 'Terverifikasi' WHEN a.status = 0 THEN 'Belum Terverifikasi' ELSE 'Undefined' END as status from po_uma.uma_tbl_users a WHERE email = '$status_email'"));


        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil mengecek status email '. $status_email);

            return response()->json(['status' => 'OK', 'rs' => $rs]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek status email '. $status_email);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function index()
    {
        return view('mpaspor.index');
    }

    public function pullData(Request $request) {

        $type       = $request->type;

        switch ($type) {
            case 'pullData':
                if ($request->has('tanggal_awal') && $request->has('tanggal_akhir')) {
                    try {
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');

                        return datatables ($this->db->select("SELECT a.nama, b.tanggal_kuota, sum(c.kuota) as total_kuota, sum(c.kuota_sisa) as sisa_kuota FROM po_kma.kma_tbl_kanim a JOIN po_kma.kma_tbl_kuota_harian b on a.id = b.kanim_id RIGHT JOIN po_kma.tbl_kuota_persesi c on b.id = c.kuota_id WHERE b.tanggal_kuota >= '$t_awal' AND b.tanggal_kuota <= '$t_akhir' GROUP BY a.id, b.id"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } else {
                    try {
            
                        $todayStart     = Carbon::now()->startOfDay();
                        $todayEnd       = Carbon::now()->endOfDay();

                        return datatables ($this->db->select("SELECT a.nama, b.tanggal_kuota, sum(c.kuota) as total_kuota, sum(c.kuota_sisa) as sisa_kuota FROM po_kma.kma_tbl_kanim a JOIN po_kma.kma_tbl_kuota_harian b on a.id = b.kanim_id RIGHT JOIN po_kma.tbl_kuota_persesi c on b.id = c.kuota_id WHERE b.tanggal_kuota >= '$todayStart' AND b.tanggal_kuota <= '$todayEnd' GROUP BY a.id, b.id"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }
                }
            break;
        }

    }

    public function kuota()
    {
        return view('mpaspor.kuota');
    }
}
