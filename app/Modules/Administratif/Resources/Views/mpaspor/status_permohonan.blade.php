@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/buttons.dataTables.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - Update Status ID Master</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">Update Status ID Master</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="col-12 col-md-12 col-lg-12">
                            <button id="btn_update" class="btn btn-primary btn-lg waves-effect waves-light" style="float: right;">Update Status Master</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kode Permohonan</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">NIK</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal Submit</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal Pengajuan</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Status Detail</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Status Master</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">ID Master</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kadaluarsa Kode Billing</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Is Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>

@push('script-footer')

    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/mpaspor/status_permohonan_app.js')}}"></script>

    <script type="text/javascript">
        var url_main        = "{{url('/administratif/mpaspor/status_permohonan')}}"
        var url_pullData    = "{{route('pullData.status_permohonan_api') }}"
        var url_api         = "{{url('api/v1/administratif/mpaspor/status_permohonan_update')}}"

    </script>

    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.status_permohonan_api') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "kode_permohonan",
                    "sClass": "text-center",
                },
                {
                    "data": "nik",
                    "sClass": "text-center",
                },
                {
                    "data": "submit_date",
                    "sClass": "text-center",
                },
                {
                    "data": "tanggal_pengajuan",
                    "sClass": "text-center",
                },
                {
                    "data": "status_detail",
                    "sClass": "text-center",
                },
                {
                    "data": "status_master",
                    "sClass": "text-center",
                },
                {
                    "data": "id_master",
                    "sClass": "text-center",
                },
                {
                    "data": "kadalauarsa_kode_billing",
                    "sClass": "text-center",
                },
                {
                    "data": "is_delete",
                    "sClass": "text-center",
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id);
            }
        });
    </script>
@endpush
@endsection