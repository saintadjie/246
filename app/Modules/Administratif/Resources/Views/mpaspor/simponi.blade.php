@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <style type="text/css">
        .titik {
          width: 100%;
          text-overflow: ellipsis;
          overflow: hidden;
        }
        .swal-wide{
            width:100% !important;
        }

        .sweet_loader {
            width: 140px;
            height: 140px;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-name: ro;
            transform-origin: 50% 50%;
            transform: rotate(0) translate(0,0);
        }
        @keyframes ro {
            100% {
                transform: rotate(-360deg) translate(0,0);
            }
        }
    </style>
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - Check Simponi</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">Check Simponi</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('home')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                
                <div class="card-body">
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">M-Paspor - Check Simponi</h5>
                                <small>Be careful!</small>
                            </div>
                            <p class="mb-1">Form ini memungkinkan anda untuk mengecek pembayaran permohonan melalui API Simponi.</p>
                            <small>Pilih Tanggal Permohonan/Kedatangan.</small>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-info">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Check Pembayaran Simponi</h5>
                                    </div>
                                    <p class="mb-1">Masukkan Tanggal Permohonan/Kedatangan yang akan dicek.</p>
                                </a>
                                <a class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <label>Tanggal Permohonan/Kedatangan</label>
                                                    <input type="text" class="form-control datepicker" id="tanggal_pengajuan" name="tanggal_pengajuan">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <button id="btn_check" class="btn btn-info btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>         
    </div>

@push('script-footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('js/administratif/mpaspor/simponi_app.js')}}"></script>
    <script type="text/javascript">
        var url_api_check   = "{{url('api/v1/administratif/mpaspor/simponi')}}"
        var url_main        = "{{url('/administratif/mpaspor/')}}"
        var sweet_loader = '<div class="sweet_loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: none; display: block; shape-rendering: auto;" width="140px" height="140px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="0" fill="none" stroke="#014c86" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate></circle><circle cx="50" cy="50" r="0" fill="none" stroke="#00a950" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.5s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.5s"></animate></circle></svg></div>';

    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            Swal.fire({
                title: "UNDER MAINTENANCE</br> (jangan digunakan dahulu)!",
                // imageUrl: ("/images/komang.jpeg"),
                // imageWidth: 100,
                // imageHeight: 100,
                // imageAlt: 'Sukses!',
                icon: 'warning',
                showClass: {
                    popup: 'animate__animated animate__jackInTheBox'
                },
                hideClass: {
                    popup: 'animate__animated animate__zoomOut'
                },
                customClass: 'swal-wide',
                allowOutsideClick : false,
                showConfirmButton: true,
                showCloseButton: true,
                backdrop: `
                    rgba(0,0,123,0.4)
                    url("../../images/nyan-cat.gif")
                    left top
                    no-repeat
                  `,
            }).then(function (result) {
                $('#kpnkb').val('')
            })
        })
    </script>

@endpush
@endsection