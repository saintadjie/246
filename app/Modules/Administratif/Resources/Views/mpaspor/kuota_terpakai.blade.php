@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/buttons.dataTables.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - Kuota</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">Kuota</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4>Custom Search :</h4>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="satker">Satuan Kerja</label>
                            <select id="satker" name="satker" class="form-control select2" multiple="">
                                @foreach($rs as $rs)
                                <option value="{{$rs->id}}" data-idname="{{$rs->nama}}">{{$rs->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="tanggal_awal">Tanggal Awal</label>
                            <input type="text" class="form-control datepicker" id="tanggal_awal" name="tanggal_awal">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="tanggal_akhir">Tanggal Akhir</label>
                            <input type="text" class="form-control datepicker" id="tanggal_akhir" name="tanggal_akhir">
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <button id="btn_cari" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Cari</button>
                        </div>
                    </div>
                    <hr/>
                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white; width: 30%;">Satker</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kuota Terpakai</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>

@push('script-footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('out/js/dataTables.buttons.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/mpaspor/kuota_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_main            = "{{url('/administratif/mpaspor/kuota')}}"
    </script>

    <script type="text/javascript">
        var table_form = $('#table_form').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],

            destroy: true,
            processing: true,
            serverSide: true,
            order: [[1, 'asc']],
            "ajax": {
                "url": "{{ route ('pullData.mpasporkuotaterpakai') }}",
                "type": "GET",
                "data": {
                    type : 'pullData',
                },
            },
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "nama",
                    "sClass": "text-center",
                },
                {
                    "data": "tanggal_pengajuan",
                    "sClass": "text-center",
                },
                {
                    "data": "kuota_terpakai",
                    "sClass": "text-center",
                },
            ],
        });


    </script>

    <script>
        $('#btn_cari').click(function(){
            var from = $("#tanggal_awal").val();
            var to = $("#tanggal_akhir").val();

            if ($('#tanggal_awal').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Tanggal Awal tidak boleh kosong", "error" )
                return false
            }
            else if ($('#tanggal_akhir').val() == '') {
                Swal.fire( "Kesalahan", "Kolom Tanggal Akhir tidak boleh kosong", "error" )
                return false
            }
            else if(Date.parse(from) > Date.parse(to)){
               Swal.fire( "Kesalahan", "Kolom Tanggal Akhir tidak boleh lebih awal dari Kolom Tanggal Awal", "error" )
                return false
            }

            
            var table_form = $('#table_form').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
                destroy: true,
                processing: true,
                serverSide: true,
                order: [[1, 'asc'], [2, 'asc']],
                "ajax": {
                    "url": "{{ route ('pullData.mpasporkuotaterpakai') }}",
                    "type": "GET",
                    "data": {
                        type : 'pullData',
                        satker          : $('#satker').val(),
                        tanggal_awal    : $('#tanggal_awal').val(),
                        tanggal_akhir    : $('#tanggal_akhir').val(),
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_pengajuan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "kuota_terpakai",
                        "sClass": "text-center",
                    },
                ],
            });

        });
    </script>

@endpush
@endsection