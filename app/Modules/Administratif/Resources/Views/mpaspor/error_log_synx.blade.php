
@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - Error Log Synx</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">Error Log Synx</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; width: 10%; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; width: 10%; color: white;">Kode Kantor</th>
                                    <th style="vertical-align: middle; text-align: center; width: 20%; color: white;">Modul</th>
                                    <th style="vertical-align: middle; text-align: center; width: 10%; color: white;">Status</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Message</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/mpaspor/')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.error_log_synx_api') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "kode_kantor",
                    "sClass": "text-center",
                },
                {
                    "data": "modul",
                    "sClass": "text-center",
                },
                {
                    "data": "status",
                    "sClass": "text-center",
                },
                {
                    "data": "message",
                    "sClass": "text-center",
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id);
            }
        });
    </script>
    
@endpush
@endsection