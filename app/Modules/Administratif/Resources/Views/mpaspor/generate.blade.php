@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/buttons.dataTables.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap-tagsinput.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - Generate Kode Billing</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">Generate Kode Billing</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="form-row">
                        <label for="tanggal_awal">Filtering</label>
                        <select id="kode_permohonan" name="kode_permohonan" class="form-control select2">
                            <option value="all" selected>Semua Data</option>
                            <option value="yes">Ada Kode Permohonan</option>
                            <option value="no">Tanpa Kode Permohonan</option>
                            <option value="kadaluarsa">Kadaluarsa <= Hari Ini</option>
                        </select>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-row" id="yes_value" hidden>
                        <div class="form-group col-12 col-md-12 col-lg-12">
                            <label for="id_permohonan_yes">Generate Kode Billing (Memiliki Kode Permohonan) dengan ID (Gunakan tanda spasi, tanda koma, atau enter untuk memisahkan ID (ID didapatkan dari nilai ID di tabel))</label>
                            <input type="text" class="form-control-lg inputtags" id="id_permohonan_yes" data-role="tagsinput" name="id_permohonan_yes">
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <button id="btn_generate_yes" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Generate</button>
                            <hr/>
                        </div>
                    </div>
                    <div class="form-row" id="no_value" hidden>
                        <div class="form-group col-12 col-md-12 col-lg-12">
                            <label for="id_permohonan_no">Generate Kode Billing dengan ID (Gunakan tanda spasi, tanda koma, atau enter untuk memisahkan ID (ID didapatkan dari nilai ID di tabel))</label>
                            <input type="text" class="form-control-lg inputtags" id="id_permohonan_no" data-role="tagsinput" name="id_permohonan_no">
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <button id="btn_generate_no" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Generate</button>
                            <hr/>
                        </div>
                    </div>
                    <div class="form-row" id="kadaluarsa_value" hidden>
                        <div class="form-group col-12 col-md-12 col-lg-12">
                            <button id="btn_generate_kadaluarsa" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Kadaluarsa</button>
                            <hr/>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kode Permohonan</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">NIK</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Nama</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal Pengajuan</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal Submit</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Status Detail</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Status Master</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Is Delete</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>

@push('script-footer')

    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('out/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('out/js/buttons.colVis.min.js')}}"></script>
    <script src="{{url('out/js/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/mpaspor/generate_app.js')}}"></script>

    <script type="text/javascript">
        var url_main                = "{{url('/administratif/mpaspor/generate')}}"
        var url_all                 = "{{route('pullData.mpasporgenerate') }}"
        var url_yes                 = "{{route('pullData.mpasporgenerateYes') }}"
        var url_no                  = "{{route('pullData.mpasporgenerateNo') }}"
        var url_api_generate_yes    = "{{url('api/v1/administratif/mpaspor/generate_yes')}}"
        var url_api_generate_no     = "{{url('api/v1/administratif/mpaspor/generate_no')}}"
        var url_api_kadaluarsa      = "{{url('api/v1/administratif/mpaspor/generate_kadaluarsa')}}"
        var date                    = "{{$date}}"

        $('input').tagsinput({
            trimValue: true,
            confirmKeys: [13, 44, 32],

        });

        // $('input').on('itemAdded', function(event) {
        //     var hehe = $("input").tagsinput('items');
        //     console.log(hehe)
        // });
    </script>
@endpush
@endsection