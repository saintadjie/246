
@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Laporan Harian Intelijen</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Laporan Harian Intelijen</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <span class="badge badge-secondary">7 Hari Terakhir {{$date}} s/d {{$today}}</span>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Satuan Kerja</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Attachment Name</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Attachment Subject</th>
                                    <th style="vertical-align: middle; text-align: center; width: 40%; color: white;">Attachment Description</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/lhi/index_app.js')}}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.12.0/dataRender/ellipsis.js"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/lhi/')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            processing: true,
                serverSide: true,
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            ajax: "{{ route ('pullData.lhi') }}",
            columnDefs: [ {
              targets: 4,
              render: $.fn.dataTable.render.ellipsis( 150, true )
            } ],
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "unit_name",
                    "sClass": "text-center",
                },
                {
                    "data": "attachment_name",
                },
                {
                    "data": "attachment_subject",
                },
                {
                    "data": "attachment_description",
                },
                {
                    "data": "id",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {

                        return '@if (auth()->user()->can('Mengubah data lhi')) <a href="{{url('/administratif/lhi/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Detail</a> @endif @if (auth()->user()->cannot('Mengubah data lhi')) Tidak memiliki akses @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id);
            }
        });
    </script>
    
@endpush
@endsection