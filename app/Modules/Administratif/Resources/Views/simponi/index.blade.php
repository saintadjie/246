@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style type="text/css">
        .titik {
          width: 100%;
          text-overflow: ellipsis;
          overflow: hidden;
        }
        .swal-wide{
            width:100% !important;
        }
    </style>
@endpush

@section('content')
<div class="section-header">
    <h1>Simponi - API</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Simponi</div>
        <div class="breadcrumb-item active"><a href="#">API</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('home')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                
                <div class="card-body">
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">API Simponi</h5>
                                <small>Be careful!</small>
                            </div>
                            <p class="mb-1">Form ini memungkinkan anda untuk mengecek pembayaran seseorang melalui API Simponi.</p>
                            <small>Masukkan Kode Billing.</small>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-info">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Check Pembayaran Simponi</h5>
                                    </div>
                                    <p class="mb-1">Masukkan Kode Billing yang akan dicek.</p>
                                </a>
                                <a class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <label>Kode Billing</label>
                                                    <input type="number" id="kode_billing" class="form-control" aria-describedby="kode_billingHelpBlock">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <button id="btn_check" class="btn btn-info btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/simponi/index_app.js')}}"></script>
    <script type="text/javascript">
        var url_api_check   = "{{url('api/v1/administratif/simponi/check')}}"
        var url_main        = "{{url('/administratif/simponi/')}}"

    </script>

@endpush
@endsection