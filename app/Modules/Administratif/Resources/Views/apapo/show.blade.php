@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
@endpush

@section('content')
<div class="section-header">
    <h1>APAPO - Kuota VIP</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">APAPO</div>
        <div class="breadcrumb-item">Kuota VIP</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Kuota VIP</a></div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/apapo')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if($rs == NULL)
                        <div class="alert alert-danger alert-has-icon">
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kesalahan!</div>
                                Kuota VIP tidak tersedia (Kuota Reguler mungkin belum dibuat)
                            </div>
                        </div>
                    @else
                    <h4 class="header-title">Daftar Kuota VIP Satuan Kerja {{$rs[0]->kantor}}</h4>
                    <p class="card-title-desc">
                        Menampilkan : <code> {{$countrs}} </code> Tanggal Permohonan.
                    </p>
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">Blast Kuota VIP</h5>
                                <small>Be careful!</small>
                            </div>
                            <p class="mb-1">Form ini memungkinkan anda untuk menambah kuota VIP secara massal.</p>
                            <small>Masukkan jumlah kuota vip, kuota tersedia/sisa kuota dan rentang waktunya.</small>
                        </a>
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="w-100 justify-content-between">
                                <div class="row">
                                    <div class="col-4 col-md-4 col-lg-4" hidden>
                                        <div class="form-group">
                                            <label>Satuan Kerja</label>
                                            <input type="text" id="satker" class="form-control" value="{{$rs[0]->id}}" aria-describedby="satkerHelpBlock" readonly>
                                        </div>
                                        
                                    </div>
                                    <div class="col-12 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label>Kuota VIP</label>
                                            <input type="number" id="quota" class="form-control" aria-describedby="quotaHelpBlock">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label>Kuota Tersedia/Sisa Kuota</label>
                                            <input type="number" id="availability" class="form-control" aria-describedby="availabilityHelpBlock">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" id="tanggal_awal" class="form-control datepicker" aria-describedby="tanggal_awalHelpBlock">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" id="tanggal_akhir" class="form-control datepicker" aria-describedby="tanggal_akhirHelpBlock">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <button id="btn_update" class="btn btn-outline-danger btn-rounded btn-lg waves-effect waves-light fas fa-recycle" style="float: right;">Update</button>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br/>
                    <div class="table-responsive">
                        <table id="tb_imigrasi" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Tanggal Permohonan</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kuota VIP</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Sisa Kuota</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kuota Terpakai</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @php $quotano = 1; @endphp
                                @php $sisano = 1; @endphp
                                @php $kuotaterpakaino = 1; @endphp
                                @foreach($rs as $rs)
                                <tr id="{{$rs->id_kuota}}">
                                    <td style="vertical-align: middle; text-align: center;">{{ $no++ }}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{$rs->tanggal_permohonan}}</td>
                                    <td style="vertical-align: middle; text-align: center;"><input type="number" min="0" id="quota" name="quota_{{$quotano++}}" value="{{$rs->quota}}" class="form-control"></td>
                                    <td style="vertical-align: middle; text-align: center;"><input type="number" min="0" id="sisa_kuota" name="sisa_kuota_{{$sisano++}}" value="{{$rs->sisa_kuota}}" class="form-control"></td>
                                    <td style="vertical-align: middle; text-align: center;"><input type="number" min="0" id="kuota_terpakai" name="kuota_terpakai_{{$kuotaterpakaino++}}" value="{{$rs->kuota_terpakai}}" class="form-control" readonly></td>
                                    <td style="vertical-align: middle; text-align: center;">
                                        <button type="button" class="btn btn-outline-danger btn-rounded btn-sm waves-effect waves-light fas fa-recycle" id="btn_simpan" data-kodevip="{{$rs->id_kuota}}"> Update</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>         
    </div>


@push('script-footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/apapo/show_app.js')}}"></script>
    <script type="text/javascript">
        var url_api         = "{{url('api/v1/administratif/apapo/store')}}"
        var url_api_update  = "{{url('api/v1/administratif/apapo/edit')}}"
        var url_main        = "{{url('/administratif/apapo/')}}"

    </script>

    <script type="text/javascript">
        $("#availability").on('blur', function(){
            if (Number($('#availability').val()) > Number($('#quota').val())) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Kesalahan',
                    text: 'Kuota Tersedia/Kuota Sisa tidak boleh lebih besar dari Kuota VIP!'
                })
                $('#quota').val('');
                $('#availability').val('');
                return false;  
            } 
        });

    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '-31d',
            endDate: '+31d',
            autoclose: true,
        });
    </script>

    <script>
        var table_form = $('#tb_imigrasi').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        });
    </script>


@endpush
@endsection