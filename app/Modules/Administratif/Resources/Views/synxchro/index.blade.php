
@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Synxchro - Nodes</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Synxchro</div>
        <div class="breadcrumb-item active"><a href="#">Nodes</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">KANIM,UKK,LTSP,ULP,RUDENIM,KANWIL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">TPI, PERWAKILAN</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <div class="table-responsive">
                                <table id="table_form_satu" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead bgcolor="DarkSlateBlue">
                                        <tr>
                                            <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Node ID</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Node Name</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Satuan Kerja</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Last Connect</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Last Connect Address</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Status 1</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Status 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for ($noSatu=1, $iSatu=0; $iSatu<$countSatu ; $noSatu++, $iSatu++)
                                            <tr>
                                                <td style="vertical-align: middle; text-align: center;"> {{$noSatu}}</td>
                                                <td>{{ $resultSatu[$iSatu]->node_id }}</td>
                                                <td>{{ $resultSatu[$iSatu]->node_name }}</td>
                                                <td>{{ $resultSatu[$iSatu]->notes }}</td>
                                                <td>{{ $resultSatu[$iSatu]->last_connect }}</td>
                                                <td>{{ $resultSatu[$iSatu]->last_connect_addr }}</td>
                                                <td>{{ $resultSatu[$iSatu]->status1 }}</td>
                                                <td>{{ $resultSatu[$iSatu]->status2 }}</td>
                                            </tr>
                        
                                        @endfor
                                    </tbody>
                                </table>                    
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div class="table-responsive">
                                <table id="table_form_dua" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead bgcolor="DarkSlateBlue">
                                        <tr>
                                            <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Node ID</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Node Name</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Satuan Kerja</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Last Connect</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Last Connect Address</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Status 1</th>
                                            <th style="vertical-align: middle; text-align: center; color: white;">Status 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for ($noDua=1, $iDua=0; $iDua<$countDua ; $noDua++, $iDua++)
                                            <tr>
                                                <td style="vertical-align: middle; text-align: center;"> {{$noDua}}</td>
                                                <td>{{ $resultDua[$iDua]->node_id }}</td>
                                                <td>{{ $resultDua[$iDua]->node_name }}</td>
                                                <td>{{ $resultDua[$iDua]->notes }}</td>
                                                <td>{{ $resultDua[$iDua]->last_connect }}</td>
                                                <td>{{ $resultDua[$iDua]->last_connect_addr }}</td>
                                                <td>{{ $resultDua[$iDua]->status1 }}</td>
                                                <td>{{ $resultDua[$iDua]->status2 }}</td>
                                            </tr>
                        
                                        @endfor
                                    </tbody>
                                </table>                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script>
        var table_form_satu = $('#table_form_satu').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],

        });
    </script>

    <script>
        var table_form_dua = $('#table_form_dua').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],

        });
    </script>
    
@endpush
@endsection