@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style type="text/css">
        .titik {
          width: 100%;
          text-overflow: ellipsis;
          overflow: hidden;
        }
        .swal-wide{
            width:100% !important;
        }
    </style>
@endpush

@section('content')
<div class="section-header">
    <h1>Dukcapil - API</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Dukcapil</div>
        <div class="breadcrumb-item active"><a href="#">API</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('home')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                
                <div class="card-body">
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">API Dukcapil</h5>
                                <small>Be careful!</small>
                            </div>
                            <p class="mb-1">Form ini memungkinkan anda untuk mengecek data seseorang melalui API Dukcapil.</p>
                            <small>Masukkan NIK dan Nama atau Tanggal Lahir.</small>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-info">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Check Data Dukcapil</h5>
                                    </div>
                                    <p class="mb-1">Masukkan NIK dan Nama atau Tanggal Lahir yang akan dicek.</p>
                                </a>
                                <a class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="w-100 justify-content-between">
                                        <div class="row">
                                            <div class="col-4 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label>NIK</label>
                                                    <input type="number" id="NIK" class="form-control" aria-describedby="NIKHelpBlock">
                                                </div>
                                            </div>
                                            <div class="col-4 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" id="NAMA_LGKP" class="form-control" aria-describedby="NAMA_LGKPHelpBlock">
                                                </div>
                                            </div>
                                            <div class="col-4 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="text" id="TGL_LHR" class="form-control datepicker" aria-describedby="TGL_LHRHelpBlock">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <button id="btn_check" class="btn btn-info btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/dukcapil/index_app.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        var url_api_check   = "{{url('api/v1/administratif/dukcapil/check')}}"
        var url_main        = "{{url('/administratif/dukcapil/')}}"

    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
        });
    </script>


@endpush
@endsection