@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="modal-content" id="modal_password">
    <div class="modal-body">
        <div class="form-group">
            <label>Password</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fas fa-lock"></i>
                    </div>
                </div>
                <input id="password" type="password" class="form-control" name="password">
            </div>
        </div>
        <div class="form-group">
            <label>Konfirmasi Password</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fas fa-lock"></i>
                    </div>
                </div>
                <input id="konfirmasi_password" type="password" class="form-control" name="konfirmasi_password">
            </div>
        </div>
    </div>
    <div class="modal-footer bg-whitesmoke br">
        <a class="btn btn-primary btn-shadow text-white mr-1" id="btn_simpan_password">Ubah Password</a>
    </div>
</div>


<div class="section-header">
    <h1>User</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">User</div>
        <div class="breadcrumb-item active"><a href="#">Ubah User</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/user')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_imigrasi">
                        <div class="container mt-5">
                            <div class="row">
                                <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h4></h4>
                                            <div class="card-header-action">
                                                <a class="btn btn-dark text-white" id="btn_password">
                                                    Ubah Password
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row" hidden>
                                                <div class="form-group col-6">
                                                    <label for="id">ID</label>
                                                    <input id="id" type="text" class="form-control" name="id" value="{{$rs->id}}">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="usernamelama">NIP Lama</label>
                                                    <input id="usernamelama" type="text" class="form-control" name="usernamelama" value="{{$rs->username}}">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="username">NIP</label>
                                                    <input id="username" type="text" class="form-control" name="username" value="{{$rs->username}}">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="nama">Nama</label>
                                                    <input id="nama" type="text" class="form-control" name="nama" value="{{$rs->nama}}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="password">Alamat</label>
                                                <textarea id="alamat" type="text" class="form-control" name="alamat">{{$rs->alamat}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Permission</label>
                                                <select id="rspermission" name="rspermission" class="form-control select2" multiple="">

                                                   

                                                </select>

                                            </div>

                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control select2" id="status" name="status">
                                                    <option value="1">Aktif</option>
                                                    <option value="0">Non Aktif</option>
                                                </select>
                                            </div>
                                
                                            <div class="form-group">
                                                <a class="btn btn-primary btn-lg btn-block text-white mr-1" id="btn_simpan">Simpan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>
</div>

@push('script-footer')
    <script src="{{url('js/administratif/user/show_app.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('out/css/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <script type="text/javascript">
        var url_api     = "{{url('api/v1/administratif/user/edit')}}";
        var url_main    = "{{url('/administratif/user/')}}";
        var url_pass    = "{{url('api/v1/administratif/user/edit_password')}}";
    </script>

    <script type="text/javascript">
        $("#btn_password").fireModal({
            title: 'UBAH PASSWORD',
            body: $("#modal_password"),
            center: true,
            autoFocus: false,
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            $.ajax({
                type: "GET",
                url: '{{url("administratif/user/pullDataRoles")}}',
                data: {
                    type        : 'pullDataRoles',
                    username    : $('#username').val()
                },
                success: function(data) {

                    $('#rspermission').empty();
                    $('option:selected', '#rspermission').remove();

                    $.each(data, function(i, item) {
                        $("#rspermission").selectpicker('destroy');
                        $('#rspermission').append($('<option></option>').val(item.name).html(item.display).data('name', item.name));
                        $("#rspermission").find("option[value='"+item.name+"']").val(item.name).html(item.display).prop("selected","selected").change();           
                    });

                    $('#rspermission').selectpicker(); 
                },
            });

            $('#status').val("{{$rs->status}}").change();

            $.ajax({
                type: "get",
                url: '{{url("administratif/user/pullDataRoles")}}',
                data: {
                    type            : 'pullDataRoles',
                    id              : $('#id').val()
                },

                success: function(data) {

                    $.each(data, function(i, item) {
                        $("#rspermission").selectpicker('destroy');
                        $('#rspermission').append($('<option></option>').val(item.name).html(item.display).data('name', item.name));
                        $('#rspermission').selectpicker();          
                    });
                }
            });
        })
    </script>


@endpush
@endsection