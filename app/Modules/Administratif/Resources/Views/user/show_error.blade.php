@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Laporan Harian Intelijen Error</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Laporan Harian Intelijen Error</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Laporan Harian Intelijen Error</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/lhi/index_error')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="header-title">Detail Laporan Harian Intelijen Error {{$rs[0]->unit_name}}</h4>
                    <hr>
                    <form id="form_penggunaan">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_lhi">Id Laporan Harian Intelijen</label>
                                <input type="text"  value="{{$rs[0]->id_lhi}}" id="id_lhi" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_penggunaan">Attachment Name</label>
                                <input type="text"  value="{{$rs[0]->attachment_name}}" id="attachment_name" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_penggunaan">Attachment Subject</label>
                                <input type="text"  value="{{$rs[0]->attachment_subject}}" id="attachment_subject" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_penggunaan">Attachment Description</label>
                                <textarea class="form-control my-editor" id="attachment_description" name="attachment_description" cols="30" rows="10">{{$rs[0]->attachment_description}}</textarea>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-10 col-md-10 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                    </form>

                    

                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/lhi/show_app.js')}}"></script>
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
 
    <script type="text/javascript">
        var url_api         = "{{url('api/v1/administratif/lhi/edit')}}"
        var url_main        = "{{url('/administratif/lhi/')}}"
    </script>

    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('attachment_description');
    </script>
@endpush
@endsection