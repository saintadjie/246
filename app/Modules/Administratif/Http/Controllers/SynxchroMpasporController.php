<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;

use Carbon\Carbon;

class SynxchroMpasporController extends Controller
{
    
    public function index()
    {
        try {
            $urlSatu = "http://api-gw.imigrasi.go.id/imi/listnode1";
            $usernameSatu = 'imigrasi';
            $passwordSatu = 'imigrasi123';

            $curlSatu = curl_init($urlSatu);
            curl_setopt($curlSatu, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($curlSatu, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlSatu, CURLOPT_USERPWD, $usernameSatu.':'.$passwordSatu);
            curl_setopt($curlSatu, CURLOPT_URL, $urlSatu);
            curl_setopt($curlSatu, CURLOPT_RETURNTRANSFER, true);

            $headersSatu = array(
               "Accept: application/json",
               "Content-Type: application/json",
            );
            curl_setopt($curlSatu, CURLOPT_HTTPHEADER, $headersSatu);


            $hasilSatu = curl_exec($curlSatu);
            curl_close($curlSatu);
            $rsSatu = json_decode($hasilSatu);

            $urlDua = "http://api-gw.imigrasi.go.id/imi/listnode2";
            $usernameDua = 'imigrasi';
            $passwordDua = 'imigrasi123';

            $curlDua = curl_init($urlDua);
            curl_setopt($curlDua, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($curlDua, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlDua, CURLOPT_USERPWD, $usernameDua.':'.$passwordDua);
            curl_setopt($curlDua, CURLOPT_URL, $urlDua);
            curl_setopt($curlDua, CURLOPT_RETURNTRANSFER, true);

            $headersDua = array(
               "Accept: application/json",
               "Content-Type: application/json",
            );
            curl_setopt($curlDua, CURLOPT_HTTPHEADER, $headersDua);


            $hasilDua = curl_exec($curlDua);
            curl_close($curlDua);
            $rsDua = json_decode($hasilDua);

            if ($rsSatu && $rsDua){
                $resultSatu = $rsSatu->result;
                $countSatu = count($rsSatu->result);
                $resultDua = $rsDua->result;
                $countDua = count($rsDua->result);
                return view('administratif::synxchro.index',['status' => 'OK', 'resultSatu' => $resultSatu, 'countSatu' => $countSatu, 'resultDua' => $resultDua, 'countDua' => $countDua]);

            } else {

                return response()->json(['status' => 'ERROR']);
            }


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }
}
