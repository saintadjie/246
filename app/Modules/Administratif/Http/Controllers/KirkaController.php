<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;

use Carbon\Carbon;

class KirkaController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('LHI');
    }

    public function pullData(Request $request) {
        

        try {
            return datatables ($this->db->select("SELECT a.ID, a.JUDUL_KIRKA, a.DESKRIPSI_KIRKA, b.UNIT_NAME, a.SUBMITED_ON FROM IMGPENGAWASAN.KIRKA_DOC a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.JUDUL_KIRKA LIKE '% %' OR a.JUDUL_KIRKA IS NULL OR a.JUDUL_KIRKA LIKE '%è%' OR a.JUDUL_KIRKA LIKE '%²%' OR a.JUDUL_KIRKA LIKE '%à%' OR a.JUDUL_KIRKA LIKE '%ò%' OR a.JUDUL_KIRKA LIKE '%±%' OR a.JUDUL_KIRKA LIKE '%°%' OR a.JUDUL_KIRKA LIKE '%´%' OR a.JUDUL_KIRKA LIKE '%À%' OR a.JUDUL_KIRKA LIKE '%ì%' OR a.JUDUL_KIRKA LIKE '%é%' OR a.JUDUL_KIRKA LIKE '%ì%' OR a.JUDUL_KIRKA LIKE '%¬%' OR a.JUDUL_KIRKA LIKE '%ñ%' OR a.JUDUL_KIRKA LIKE '%č%' OR a.JUDUL_KIRKA LIKE '%ž%' OR a.JUDUL_KIRKA LIKE '%á%' OR a.JUDUL_KIRKA LIKE '%ě%' OR a.JUDUL_KIRKA LIKE '%Ú%' OR a.JUDUL_KIRKA LIKE '%í%' OR a.JUDUL_KIRKA LIKE '%Č%' OR a.JUDUL_KIRKA LIKE '%ý%' OR a.JUDUL_KIRKA LIKE '%×%' OR a.JUDUL_KIRKA LIKE '%ö%' OR a.JUDUL_KIRKA LIKE '%ü%' OR a.JUDUL_KIRKA LIKE '%ı%' OR a.JUDUL_KIRKA LIKE '%ù%' OR a.JUDUL_KIRKA LIKE '%ç%' OR a.JUDUL_KIRKA LIKE '%»%' OR a.JUDUL_KIRKA LIKE '%£%' OR a.JUDUL_KIRKA LIKE '%È%' OR a.JUDUL_KIRKA LIKE '%Ù%' OR a.JUDUL_KIRKA LIKE '%⁹%' OR a.JUDUL_KIRKA LIKE '%­%' OR a.JUDUL_KIRKA LIKE '%¹%' OR a.JUDUL_KIRKA LIKE '%Ö%' OR a.JUDUL_KIRKA LIKE '%þ%' OR a.JUDUL_KIRKA LIKE '%⅝%' OR a.JUDUL_KIRKA LIKE '%ó%' OR a.DESKRIPSI_KIRKA LIKE '% %' OR a.DESKRIPSI_KIRKA IS NULL OR a.DESKRIPSI_KIRKA LIKE '%è%' OR a.DESKRIPSI_KIRKA LIKE '%²%' OR a.DESKRIPSI_KIRKA LIKE '%à%' OR a.DESKRIPSI_KIRKA LIKE '%ò%' OR a.DESKRIPSI_KIRKA LIKE '%±%' OR a.DESKRIPSI_KIRKA LIKE '%°%' OR a.DESKRIPSI_KIRKA LIKE '%´%' OR a.DESKRIPSI_KIRKA LIKE '%À%' OR a.DESKRIPSI_KIRKA LIKE '%ì%' OR a.DESKRIPSI_KIRKA LIKE '%é%' OR a.DESKRIPSI_KIRKA LIKE '%ì%' OR a.DESKRIPSI_KIRKA LIKE '%¬%' OR a.DESKRIPSI_KIRKA LIKE '%ñ%' OR a.DESKRIPSI_KIRKA LIKE '%č%' OR a.DESKRIPSI_KIRKA LIKE '%ž%' OR a.DESKRIPSI_KIRKA LIKE '%á%' OR a.DESKRIPSI_KIRKA LIKE '%ě%' OR a.DESKRIPSI_KIRKA LIKE '%Ú%' OR a.DESKRIPSI_KIRKA LIKE '%í%' OR a.DESKRIPSI_KIRKA LIKE '%Č%' OR a.DESKRIPSI_KIRKA LIKE '%ý%' OR a.DESKRIPSI_KIRKA LIKE '%×%' OR a.DESKRIPSI_KIRKA LIKE '%ö%' OR a.DESKRIPSI_KIRKA LIKE '%ü%' OR a.DESKRIPSI_KIRKA LIKE '%ı%' OR a.DESKRIPSI_KIRKA LIKE '%ù%' OR a.DESKRIPSI_KIRKA LIKE '%ç%' OR a.DESKRIPSI_KIRKA LIKE '%»%' OR a.DESKRIPSI_KIRKA LIKE '%£%' OR a.DESKRIPSI_KIRKA LIKE '%È%' OR a.DESKRIPSI_KIRKA LIKE '%Ù%' OR a.DESKRIPSI_KIRKA LIKE '%⁹%' OR a.DESKRIPSI_KIRKA LIKE '%­%' OR a.DESKRIPSI_KIRKA LIKE '%¹%' OR a.DESKRIPSI_KIRKA LIKE '%Ö%' OR a.DESKRIPSI_KIRKA LIKE '%þ%' OR a.DESKRIPSI_KIRKA LIKE '%⅝%' OR a.DESKRIPSI_KIRKA LIKE '%ó%'"))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function index(Request $request) {
        
        return view('administratif::kirka.index');

    }
    
    public function page_show(Request $request, $id) {
        
        $id         = $request->id;

        $rs         = $this->db->select(DB::raw("SELECT a.ID as id_lhi, a.JUDUL_KIRKA, a.DESKRIPSI_KIRKA, b.UNIT_NAME, a.SUBMITED_ON FROM IMGPENGAWASAN.KIRKA_DOC a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.ID = '$id'"));
        
        // dd($rs);
        return view('administratif::kirka.show', ['rs' => $rs]);

    }

}
