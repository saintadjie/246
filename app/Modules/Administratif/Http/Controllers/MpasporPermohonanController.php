<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class MpasporPermohonanController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function pullData(Request $request) {

        $type       = $request->type;

        switch ($type) {
            case 'pullData':
                if ($request->has('satker') && $request->has('tanggal_awal') && $request->has('tanggal_akhir') && $request->has('status_detail') && $request->has('status_master')) {
                    try {
                        $satker         = $request->satker;
                        $satker         = implode(",",$satker);
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');
                        $status_detail  = $request->status_detail;
                        $status_master  = $request->status_master;

                        return datatables ($this->db->select("SELECT a.tanggal_pengajuan, d.nama_lengkap as nama_akun, c.email, c.nomor_telepon, b.nama as nama_pemohon, e.nama as satker FROM po_mpp.mpp_tbl_permohonan a JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id join po_uma.uma_tbl_users c ON a.user_id = c.id join po_uma.uma_tbl_user_profile d ON c.id = d.user_id join po_kma.kma_tbl_kanim e ON e.id = a.kanim_id where a.kanim_id IN ($satker) AND a.tanggal_pengajuan between '$tanggal_awal' AND '$tanggal_akhir' AND b.status = '$status_detail' AND a.status = '$status_master';"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } elseif ($request->has('tanggal_awal') && $request->has('tanggal_akhir') && $request->has('status_detail') && $request->has('status_master')) {
                    try {
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');
                        $status_detail  = $request->status_detail;
                        $status_master  = $request->status_master;

                        return datatables ($this->db->select("SELECT a.tanggal_pengajuan, d.nama_lengkap as nama_akun, c.email, c.nomor_telepon, b.nama as nama_pemohon, e.nama as satker FROM po_mpp.mpp_tbl_permohonan a JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id join po_uma.uma_tbl_users c ON a.user_id = c.id join po_uma.uma_tbl_user_profile d ON c.id = d.user_id join po_kma.kma_tbl_kanim e ON e.id = a.kanim_id where a.tanggal_pengajuan between '$tanggal_awal' AND '$tanggal_akhir' AND b.status = '$status_detail' AND a.status = '$status_master';"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } else {
                    try {
            
                        $todayStart     = Carbon::now()->startOfDay();
                        $todayEnd       = Carbon::now()->endOfDay();

                        return datatables ($this->db->select("SELECT a.tanggal_pengajuan, d.nama_lengkap as nama_akun, c.email, c.nomor_telepon, b.nama as nama_pemohon, e.nama as satker FROM po_mpp.mpp_tbl_permohonan a JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id join po_uma.uma_tbl_users c ON a.user_id = c.id join po_uma.uma_tbl_user_profile d ON c.id = d.user_id join po_kma.kma_tbl_kanim e ON e.id = a.kanim_id where a.tanggal_pengajuan between '$todayStart' AND '$todayEnd' AND b.status = '3' AND a.status = '3';"))
                        ->addIndexColumn()
                        ->toJson(10);


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }
                }
            break;
        }

    }

    public function index()
    {

        $rs         = $this->db->select(DB::raw("SELECT id, nama FROM po_kma.kma_tbl_kanim"));

        return view('administratif::mpaspor.permohonan', ['rs' => $rs]);

    }
}


