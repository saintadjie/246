<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class MpasporKuotaController extends Controller
{
    
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function pullData(Request $request) {

        $type       = $request->type;

        switch ($type) {
            case 'pullData':
                if ($request->has('satker') && $request->has('tanggal_awal') && $request->has('tanggal_akhir')) {
                    try {
                        $satker         = $request->satker;
                        $satker         = implode(",",$satker);
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');

                        return datatables ($this->db->select("SELECT a.nama, b.tanggal_kuota, sum(c.kuota) as total_kuota, sum(c.kuota_sisa) as sisa_kuota FROM po_kma.kma_tbl_kanim a JOIN po_kma.kma_tbl_kuota_harian b on a.id = b.kanim_id RIGHT JOIN po_kma.tbl_kuota_persesi c on b.id = c.kuota_id WHERE a.id in ($satker) AND b.tanggal_kuota >= '$t_awal' AND b.tanggal_kuota <= '$t_akhir' GROUP BY a.id, b.id"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } elseif ($request->has('tanggal_awal') && $request->has('tanggal_akhir')) {
                    try {
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');

                        return datatables ($this->db->select("SELECT a.nama, b.tanggal_kuota, sum(c.kuota) as total_kuota, sum(c.kuota_sisa) as sisa_kuota FROM po_kma.kma_tbl_kanim a JOIN po_kma.kma_tbl_kuota_harian b on a.id = b.kanim_id RIGHT JOIN po_kma.tbl_kuota_persesi c on b.id = c.kuota_id WHERE b.tanggal_kuota >= '$t_awal' AND b.tanggal_kuota <= '$t_akhir' GROUP BY a.id, b.id"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } else {
                    try {
            
                        $todayStart     = Carbon::now()->startOfDay();
                        $todayEnd       = Carbon::now()->endOfDay();

                        return datatables ($this->db->select("SELECT a.nama, b.tanggal_kuota, sum(c.kuota) as total_kuota, sum(c.kuota_sisa) as sisa_kuota FROM po_kma.kma_tbl_kanim a JOIN po_kma.kma_tbl_kuota_harian b on a.id = b.kanim_id RIGHT JOIN po_kma.tbl_kuota_persesi c on b.id = c.kuota_id WHERE b.tanggal_kuota >= '$todayStart' AND b.tanggal_kuota <= '$todayEnd' GROUP BY a.id, b.id"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }
                }
            break;
        }

    }

    public function index()
    {
        $rs         = $this->db->select(DB::raw("SELECT id, nama FROM po_kma.kma_tbl_kanim"));

        return view('administratif::mpaspor.kuota', ['rs' => $rs]);
    }
}
