<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Auth;

class MpasporStatusController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function status_permohonan_api(Request $request) {

        try {

            $date   = Carbon::now()->startOfDay();
            $now    = Carbon::now();


            return datatables ($this->db->select("SELECT a.kode_permohonan, a.nik, b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.id as id_detail, b.id as id_master, a.kadalauarsa_kode_billing, a.is_delete FROM po_mpp.mpp_tbl_detail_booking_pemohon a JOIN po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id WHERE a.status = 3 AND a.is_delete in (0) AND b.tanggal_pengajuan >= '$date' AND a.kadalauarsa_kode_billing <= '$now' AND b.master_document_link is not NULL AND b.status in (2)"))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function update_status(Request $request)
    {

        $usernames  = Auth::User()->username;
        $date       = Carbon::now()->startOfDay();
        $now        = Carbon::now();

        $rs         = $this->db->select("SELECT b.id as id_master FROM po_mpp.mpp_tbl_detail_booking_pemohon a JOIN po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id WHERE a.status = 3 AND a.is_delete in (0) AND b.tanggal_pengajuan >= '$date' AND a.kadalauarsa_kode_billing <= '$now' AND b.master_document_link is not NULL AND b.status in (2)");

        $length     = count($rs);
        $generate   = '';
        for ($i = 0; $i < $length; $i++) {
            $generate = $generate . $rs[$i]->id_master . ',';
            if ($i == $length-1) {
              $generate = $generate . $rs[$i]->id_master;
            }
        }

        $id_master  = $generate;
        
        $result             = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '3' WHERE id IN ($id_master)"));
     
        if ($result){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil update ID Master '. $id_master);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal update ID Master '. $id_master);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function status_permohonan()
    {

        return view('administratif::mpaspor.status_permohonan');
    }

}


