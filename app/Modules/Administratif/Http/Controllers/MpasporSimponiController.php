<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Auth;

class MpasporSimponiController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function check(Request $request)
    {

        $tanggal_pengajuan  = $request->tanggal_pengajuan;
        $now                = Carbon::now();
        $usernames          = Auth::User()->username;

        $rs         = $this->db->select(DB::raw("SELECT a.id FROM po_mpp.mpp_tbl_detail_booking_pemohon a JOIN po_mpp.mpp_tbl_permohonan b ON a.permohonan_id = b.id WHERE a.status = 2 AND a.is_delete = 0 AND tanggal_pengajuan = '$tanggal_pengajuan' and kadalauarsa_kode_billing <= '$now'"));
        $rsid  = [];

        foreach ($rs as $rs) {
            $rsid[] =  $rs->id;
        }

        $json_id         = implode(",",$rsid);

        if ($json_id){

            $url                = "http://10.20.66.13:8089/api/simponi/bridge/check-billing";

            $curl   = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
               "Accept: application/json",
               "Content-Type: application/json",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $data = '{"id": [
                        '.$json_id.'
                    ]}';

            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

            $hasil = curl_exec($curl);
            curl_close($curl);
            $rsjson_id = json_decode($hasil);

            $rsid2  = '';
            foreach ($rsjson_id->data->list_response as $key => $value) {
                if($value != null){
                    $rsid2 = '\'' . $key . '\',' . $rsid2; 
                }
                
            }

            $rsid2 = rtrim($rsid2,',');

            if($rsid2 != ''){
                $rsx         = $this->db->select(DB::raw("SELECT a.id FROM po_mpp.mpp_tbl_detail_booking_pemohon a WHERE a.kode_permohonan IN ($rsid2)"));

                $rsxid  = [];

                foreach ($rsx as $rsx) {
                    $rsxid[] =  $rsx->id;
                }

                $json_idx         = implode(",",$rsxid);

                if ($json_idx){

                    $urlx = "http://10.20.66.13:8089/api/simponi/bridge/check-billing-sync";

                    $curlx   = curl_init($urlx);
                    curl_setopt($curlx, CURLOPT_URL, $urlx);
                    curl_setopt($curlx, CURLOPT_SSL_VERIFYPEER, false); 
                    curl_setopt($curlx, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curlx, CURLOPT_POST, true);
                    curl_setopt($curlx, CURLOPT_RETURNTRANSFER, true);

                    $headersx = array(
                       "Accept: application/json",
                       "Content-Type: application/json",
                    );
                    curl_setopt($curlx, CURLOPT_HTTPHEADER, $headersx);

                    $datax = '{"id": [
                                '.$json_idx.'
                            ]}';

                    curl_setopt($curlx, CURLOPT_POSTFIELDS, $datax);

                    $hasilx = curl_exec($curlx);
                    curl_close($curlx);
                    $rsjson_idx = json_decode($hasilx);

                    $usernames              = Auth::User()->username;
                    activity()
                    ->withProperties(['username' => $usernames])
                    ->log($usernames. ' berhasil mengecek simponi permohonan '. $tanggal_pengajuan);

                    return response()->json(['status' => 'OK']);

                } else {

                    $usernames              = Auth::User()->username;
                    activity()
                    ->withProperties(['username' => $usernames])
                    ->log($usernames. ' gagal mengecek simponi permohonan '. $tanggal_pengajuan);

                    return response()->json(['status' => 'ERROR']);

                }
            } else {

                $usernames              = Auth::User()->username;
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal mengecek simponi permohonan '. $tanggal_pengajuan);

                return response()->json(['status' => 'ERROR']);
            }

        } else {

            $usernames              = Auth::User()->username;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek simponi permohonan '. $tanggal_pengajuan);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function index()
    {

        return view('administratif::mpaspor.simponi');
    }
}

