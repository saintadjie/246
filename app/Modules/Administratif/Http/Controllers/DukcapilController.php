<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Auth;

class DukcapilController extends Controller
{
    public function index()
    {

        return view('administratif::dukcapil.index');
    }


    public function check(Request $request)
    {

        $nik            = $request->NIK;
        $nama           = $request->NAMA_LGKP;
        $tanggallahir   = $request->TGL_LHR;

        $url            = "http://10.0.2.6/dukcapil/nik_verifby_elemen";
        $username       = 'pasporonline';
        $password       = 'pasporonline';

        $usernames      = Auth::User()->username;


        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
           'Authorization: Basic '. base64_encode($username.':'.$password),
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"NIK":"'.$nik.'","NAMA_LGKP":"'.$nama.'","TGL_LHR":"'.$tanggallahir.'","TRESHOLD":"90"}';


        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);
        curl_close($curl);
        $rs = json_decode($hasil);

        if (isset($rs->content[0]->NIK)) {

            $result     = $rs->content[0];
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek NIK '. $nik);

            return response()->json(['status' => 'OK', 'result' => $result]);
        } else {

            $rsmessage = $rs->content[0]->RESPONSE;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek NIK '. $nik);

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }
    }
}
