<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Auth;

class SimponiController extends Controller
{

    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function index()
    {

        return view('administratif::simponi.index');
    }

    public function check(Request $request)
    {

        $url = "http://10.0.22.103:8088/simponi/trx/inquiry";
        $uid = '095f357bb1885755-c3be8f09-9a48819d';
        $kode_billing = $request->kode_billing;
        $usernames = Auth::User()->username;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
           "uid: $uid",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);



        $data = '{"method": "inquirybilling_v2", 
                "data": [
                    "202112071635140000000",
                    "121917",
                    "SIMKIM9i4a",
                    "'.$kode_billing.'", 
                    "013",
                    "06",
                    "409272"
                ]}';

        $datampaspor         = $this->db->select(DB::raw("SELECT a.nama from po_mpp.mpp_tbl_detail_booking_pemohon a where a.simponi_billing_code = '$kode_billing'"));

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);


        curl_close($curl);
        $rs = json_decode($hasil);
        $rsstatus = $rs->response;
        
        if ($rsstatus->code == '00'){
            if(empty($datampaspor[0])){
                $result = $rs;
                $rsmessage = $rs->response->message;
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' melakukan pengecekan simponi dengan Kode Billing '. $kode_billing);

                return response()->json(['status' => 'OK', 'result' => $result, 'rsmessage' => $rsmessage, 'datampaspor' => '-']);
            } else {
                $result = $rs;
                $rsmessage = $rs->response->message;
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' melakukan pengecekan simponi dengan Kode Billing '. $kode_billing);

                return response()->json(['status' => 'OK', 'result' => $result, 'rsmessage' => $rsmessage, 'datampaspor' => $datampaspor[0]->nama]);
            }

        } else {
            $rsmessage = $rs->response->message;
            activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal melakukan pengecekan simponi dengan Kode Billing '. $kode_billing);

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }
    }
}
