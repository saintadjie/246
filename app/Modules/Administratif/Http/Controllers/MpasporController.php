<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Auth;

class MpasporController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function error_log_synx_api(Request $request) {

        try {
            
            return datatables ($this->db->select('SELECT * FROM po_boi.mpaspor_log_synx where message is not null'))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function error_log_synx()
    {

        return view('administratif::mpaspor.error_log_synx');
    }

    public function get_permohonan(Request $request)
    {

        $kpnkb      = $request->kpnkb;
        $usernames  = Auth::User()->username;

        $rs         = $this->db->select(DB::raw("SELECT a.id, a.kode_permohonan, b.id as id_master, a.nik, a.nama, d.nama as satker, a.status as status_detail, b.status as status_permohonan, a.simponi_billing_code, a.kadalauarsa_kode_billing, a.simponi_ntb, a.simponi_ntpn, b.submit_date as tanggal_submit, b.tanggal_pengajuan as tanggal_kedatangan, c.nama_sesi, a.ready_to_sync, a.compiled_at, a.is_delete, a.is_hold, b.rescheduled_count, CASE WHEN b.rescheduled_count = 1 THEN 'Tidak' ELSE 'Ya' END AS rescheduled, b.master_document_link from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on b.id = a.permohonan_id join po_kma.tbl_kuota_persesi c on b.sesi = c.id join po_kma.kma_tbl_kanim d on d.id = b.kanim_id where a.nik = '$kpnkb' or a.kode_permohonan = '$kpnkb' or a.simponi_billing_code = '$kpnkb'"));

        if ($rs){

            foreach ($rs as $key => $value) {
                $data = array_search($value->id, array_column($rs, 'id'));
                
                $id = $rs[$data]->id;
                $value->kode_permohonan = $rs[$data]->kode_permohonan;
                $value->id_master = $rs[$data]->id_master;
                $value->nik = $rs[$data]->nik;
                $value->nama = $rs[$data]->nama;
                $value->satker = $rs[$data]->satker;
                $value->status_detail =$rs[$data]->status_detail;
                $value->status_permohonan = $rs[$data]->status_permohonan;
                $value->simponi_billing_code = $rs[$data]->simponi_billing_code;
                $value->kadalauarsa_kode_billing = $rs[$data]->kadalauarsa_kode_billing;
                $value->simponi_ntb = $rs[$data]->simponi_ntb;
                $value->simponi_ntpn = $rs[$data]->simponi_ntpn;
                $value->tanggal_submit = $rs[$data]->tanggal_submit;
                $value->tanggal_kedatangan = $rs[$data]->tanggal_kedatangan;
                $value->nama_sesi = $rs[$data]->nama_sesi;
                $value->ready_to_sync = $rs[$data]->ready_to_sync;
                $value->compiled_at = $rs[$data]->compiled_at;
                $value->is_delete = $rs[$data]->is_delete;
                $value->is_hold = $rs[$data]->is_hold;
                $value->master_document_link = $rs[$data]->master_document_link;

            }

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek data permohonan NIK/Kode Permohonan/Kode Billing '. $kpnkb);

            return response()->json(['status' => 'OK', 'rs' => $rs]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek data permohonan NIK/Kode Permohonan/Kode Billing '. $kpnkb);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function update_detail (Request $request)
    {

        $id         = $request->id;
        $is_delete  = $request->is_delete;
        $is_hold    = $request->is_hold;
        $usernames  = Auth::User()->username;

        $rsupdate         = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_detail_booking_pemohon SET is_delete = '$is_delete', is_hold = '$is_hold' WHERE id = '$id'"));


        if ($rsupdate){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil mengubah detail permohonan '. $id);

            return response()->json(['status' => 'OK', 'rsupdate' => $rsupdate]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengubah detail permohonan '. $id);

            return response()->json(['status' => 'ERROR']);

        }

    }


    public function refresh(Request $request)
    {

        $kode_permohonan_refresh    = $request->kode_permohonan_refresh;
        $usernames                  = Auth::User()->username;
        $url                        = "http://10.20.66.5:8080/validator/application-code";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"passport_application_id_list": [
                    "'.$kode_permohonan_refresh.'"
                ]}';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);
        curl_close($curl);
        $rs = json_decode($hasil);

        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' merefresh Kode Billing dan Kode Permohonan dengan detail ID permohonan '. $kode_permohonan_refresh);

            return response()->json(['status' => 'OK']);

        } else {
            
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal merefresh Kode Billing dan Kode Permohonan dengan detail ID permohonan '. $kode_permohonan_refresh);

            return response()->json(['status' => 'ERROR']);
        }

    }

    
    public function reset(Request $request)
    {

        $kode_permohonan    = $request->kode_permohonan_reset;
        $usernames          = Auth::User()->username;
        $url                = "http://10.20.66.6:8071/api/cms/mpp/passport-application/retry-payment";

        $rstoken            = $this->db->select(DB::raw("SELECT a.uuid_token FROM po_uma.uma_jwt_token a join po_uma.uma_tbl_users b on a.user_id = b.id WHERE a.is_expired = '0' and b.role_id = '2' ORDER BY a.expired_at desc LIMIT 1"));

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Authorization: Bearer ". $rstoken[0]->uuid_token,
           "Content-Type: application/json",
        );


        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"kode_permohonan":"'.$kode_permohonan.'"}';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil      = curl_exec($curl);
        curl_close($curl);
        $rs         = json_decode($hasil);        
        $rsstatus   = $rs->status;

        if ($rsstatus == 'true'){

            $rsdata = $rs->data;
            $rskedatangan = $rsdata->booking_date;
            $rsdetail = $rsdata->passport_application_detail[0];
            $rsmessage = $rs->message;
            $rsid = $rsdetail->id;
            $rsnama = $rsdetail->name;
            $rssimponi = $rsdetail->simponi_billing_code;

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mereset Kode Permohonan '. $kode_permohonan);

            return response()->json(['status' => 'OK', 'rsmessage' => $rsmessage, 'rsid' => $rsid, 'rsnama' => $rsnama, 'rskedatangan' => $rskedatangan, 'rssimponi' => $rssimponi]);

        } else {
            $rsmessage = $rs->message;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mereset Kode Permohonan '. $kode_permohonan);

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }

    }

    public function check(Request $request)
    {

        $id_check   = $request->id_check;
        $usernames  = Auth::User()->username;
        $url        = "http://10.20.66.2:8071/api/callback/simponi/check-bill-queued";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"passport_application_detail_id":"'.$id_check.'"}';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);
        curl_close($curl);
        $rs = json_decode($hasil);
        $rsstatus = $rs->status;


        if ($rsstatus == 'true'){
            $rskodepermohonan = $this->db->select(DB::raw("SELECT a.kode_permohonan, b.tanggal_pengajuan, a.status as status_detail, b.status as status_permohonan from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on b.id = a.permohonan_id where a.id = '$id_check'"));
            $rsmessage = $rs->message;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek pembayaran simponi dengan detail ID Permohonan '. $id_check);

            return response()->json(['status' => 'OK', 'rsmessage' => $rsmessage,'rskodepermohonan' => $rskodepermohonan]);

        } else {
            $rsmessage = $rs->message;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek pembayaran simponi dengan detail ID Permohonan '. $id_check);

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }

    }

    public function compile(Request $request)
    {

        $kode_permohonan    = $request->kode_permohonan_compile;
        $usernames          = Auth::User()->username;
        $url                = "http://10.20.66.2:8071/api/callback/synxchro/compile-mirror-table-per-application-code";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"application_code":"'.$kode_permohonan.'"}';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);
        curl_close($curl);
        $rs = json_decode($hasil);
        $rsstatus = $rs->status;

        if ($rsstatus == 'true'){
            $rsmessage = $rs->message;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengcompile Kode Permohonan '. $kode_permohonan);

            return response()->json(['status' => 'OK', 'rsmessage' => $rsmessage]);

        } else {
            $rsmessage = $rs->message;
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengcompile Kode Permohonan '. $kode_permohonan);

            return response()->json(['status' => 'ERROR', 'rsmessage' => $rsmessage]);
        }

    }

    public function kadaluarsa(Request $request)
    {

        $kode_permohonan_kadaluarsa = $request->kode_permohonan_kadaluarsa;
        $usernames                  = Auth::User()->username;

        $rsview         = $this->db->select(DB::raw("SELECT a.id, a.kode_permohonan, a.nik, a.nama, a.permohonan_id from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on b.id = a.permohonan_id where a.id = '$kode_permohonan_kadaluarsa'"));


        if(empty($rsview[0])){
           
            return response()->json(['status' => 'INVALID']);

        } else {

            $rs_id = $rsview[0]->permohonan_id;

            $rsupdate         = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_detail_booking_pemohon SET status = '4' WHERE id = '$kode_permohonan_kadaluarsa'"));
            $rsupdate2        = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '4' WHERE id = '$rs_id'"));


            if ($rsupdate2){

                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' membuat kadaluarsa detail ID Permohonan '. $kode_permohonan_kadaluarsa);

                return response()->json(['status' => 'OK']);

            } else {

                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal membuat kadaluarsa detail ID Permohonan '. $kode_permohonan_kadaluarsa);

                return response()->json(['status' => 'ERROR']);

            }
        }

    }

    public function check_reschedule(Request $request)
    {

        $kode_permohonan_reschedule = $request->kode_permohonan_reschedule;
        $usernames                  = Auth::User()->username;

        $rs         = $this->db->select(DB::raw("SELECT a.nik, a.kode_permohonan, a.nama, b.create_time, (select nama from po_kma.kma_tbl_kanim where id = b.before_kanim_id) as KANIM_SEBELUM, (select nama from po_kma.kma_tbl_kanim where id = b.after_kanim_id) as KANIM_SESUDAH, (select nama_sesi from po_kma.tbl_kuota_persesi where id = b.before_session_id) as SESI_SEBELUM, (select nama_sesi from po_kma.tbl_kuota_persesi where id = b.after_session_id) as SESI_SESUDAH, (select tanggal_kuota from po_kma.kma_tbl_kuota_harian where id = c.kuota_id) as TANGGAL_SEBELUM, (select tanggal_kuota from po_kma.kma_tbl_kuota_harian where id = d.kuota_id) as TANGGAL_SESUDAH from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_reschedule_history b on a.permohonan_id = b.passport_application_id join po_kma.tbl_kuota_persesi c on b.before_session_id = c.id join po_kma.tbl_kuota_persesi d on b.after_session_id = d.id where a.kode_permohonan = '$kode_permohonan_reschedule'"));

        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek data reschedule Kode Permohonan '. $kode_permohonan_reschedule);

            return response()->json(['status' => 'OK', 'rs' => $rs]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek data reschedule Kode Permohonan '. $kode_permohonan_reschedule);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function boi(Request $request)
    {

        $kode_permohonan_boi    = $request->kode_permohonan_boi;
        $usernames              = Auth::User()->username;

        $rs_tx_paspor              = $this->db->select(DB::raw("SELECT kode_permohonan, tanggal_permohonan, kode_billing, id_satuan_kerja, sts_sync, dtm_last_updated from po_boi.tx_paspor a where a.kode_permohonan = '$kode_permohonan_boi'"));

        if ($rs_tx_paspor){

            $rs_tx_informasi_pemohon        = $this->db->select(DB::raw("SELECT kode_permohonan, nama_di_paspor, nik, id_status_sipil, sts_sync from po_boi.tx_informasi_pemohon a where a.kode_permohonan = '$kode_permohonan_boi'"));
            $rs_tx_informasi_keluarga       = $this->db->select(DB::raw("SELECT kode_permohonan, status_keluarga, nama_keluarga, sts_sync from po_boi.tx_informasi_keluarga a where a.kode_permohonan = '$kode_permohonan_boi'"));
            $rs_tx_kelengkapan_dokumen      = $this->db->select(DB::raw("SELECT kode_permohonan, data_file, sts_sync from po_boi.tx_kelengkapan_dokumen a where a.kode_permohonan = '$kode_permohonan_boi'"));
            $rs_tx_pembayaran_dpri          = $this->db->select(DB::raw("SELECT kode_permohonan, nama_di_paspor, status, id_bank, id_satuan_kerja, sts_sync from po_boi.tx_pembayaran_dpri a where a.kode_permohonan = '$kode_permohonan_boi'"));
            $rs_tx_detail_pembayaran_dpri   = $this->db->select(DB::raw("SELECT kode_permohonan, id_pembayaran_dpri, sts_sync from po_boi.tx_detail_pembayaran_dpri a where a.kode_permohonan = '$kode_permohonan_boi'"));
            $rs_tx_pembayaran_simponi       = $this->db->select(DB::raw("SELECT kode_permohonan, kode_billing, trx_id, sts_sync from po_boi.tx_pembayaran_simponi a where a.kode_permohonan = '$kode_permohonan_boi'"));

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengecek data tx boi Kode Permohonan '. $kode_permohonan_boi);

            return response()->json(['status' => 'OK', 'rs_tx_paspor' => $rs_tx_paspor, 'rs_tx_detail_pembayaran_dpri' => $rs_tx_detail_pembayaran_dpri, 'rs_tx_informasi_keluarga' => $rs_tx_informasi_keluarga, 'rs_tx_informasi_pemohon' => $rs_tx_informasi_pemohon, 'rs_tx_kelengkapan_dokumen' => $rs_tx_kelengkapan_dokumen, 'rs_tx_pembayaran_dpri' => $rs_tx_pembayaran_dpri, 'rs_tx_pembayaran_simponi' => $rs_tx_pembayaran_simponi]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek data tx boi Kode Permohonan '. $kode_permohonan_boi);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function status_email(Request $request)
    {

        $status_email   = $request->status_email;
        $usernames      = Auth::User()->username;

        $rs             = $this->db->select(DB::raw("SELECT a.id, a.email, a.nomor_telepon, a.status, a.is_delete, CASE WHEN NOT EXISTS (SELECT id FROM po_uma.uma_tbl_user_profile WHERE user_id = a.id ) Then 'No' Else 'Yes' END As availability from po_uma.uma_tbl_users a where email = '$status_email'"));


        if ($rs){

            $rscount = $this->db->select(DB::raw("SELECT count(*) as total from po_uma.uma_tbl_users WHERE email = '$status_email'"));
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil mengecek status email '. $status_email);

            return response()->json(['status' => 'OK', 'rs' => $rs, 'rscount' => $rscount]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengecek status email '. $status_email);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function status_email_update(Request $request)
    {

        $email_id               = $request->email_id;
        $email_email            = $request->email_email;
        $email_nomor_telepon    = $request->email_nomor_telepon;
        $email_status           = $request->email_status;
        $email_is_delete        = $request->email_is_delete;

        $usernames              = Auth::User()->username;

        $rsupdate               = $this->db->update(DB::raw("UPDATE po_uma.uma_tbl_users SET email = '$email_email', nomor_telepon = '$email_nomor_telepon', status = '$email_status', is_delete = '$email_is_delete' WHERE id = '$email_id'"));


        if ($rsupdate){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil mengubah detail email '. $email_email);

            return response()->json(['status' => 'OK', 'rsupdate' => $rsupdate]);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengubah detail email '. $email_email);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function gasssss(Request $request)
    {

        $today              = Carbon::now()->format('Y-m-d');
        $kode_permohonan    = $this->db->select(DB::raw("SELECT a.kode_permohonan from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where b.tanggal_pengajuan = '$today' and a.status in (3) and a.ready_to_sync in (0) LIMIT 3"));
        
        $length     = count($kode_permohonan);

        if ($length == 0) {
            return response()->json(['status' => 'EMPTY']);
        } else {
            $generate   = '';

            for ($i = 0; $i < $length; $i++) {
                $generate = $generate . $kode_permohonan[$i]->kode_permohonan . ',';

                if ($i == $length-2) {
                  $generate = $generate . $kode_permohonan[$i]->kode_permohonan;
                  break;
                }
            }
            $kode_permohonan    = $generate;
            $kode_permohonan    = explode(',', $kode_permohonan);

            foreach($kode_permohonan as $i =>$key) {

                $usernames = Auth::User()->username;
                $url = "http://10.20.66.2:8071/api/callback/synxchro/compile-mirror-table-per-application-code";

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $headers = array(
                   "Accept: application/json",
                   "Content-Type: application/json",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

                $data = '{"application_code":"'.$key.'"}';

                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                $hasil = curl_exec($curl);
                curl_close($curl);
                $rs = json_decode($hasil);
                $rsstatus = $rs->status;

                if ($rsstatus == 'true'){
                    $rsmessage = $rs->message;
                    activity()
                    ->withProperties(['username' => $usernames])
                    ->log('System berhasil mengcompile Kode Permohonan '. $key);


                } else {
                    $rsmessage = $rs->message;
                    activity()
                    ->withProperties(['username' => $usernames])
                    ->log('System gagal mengcompile Kode Permohonan '. $key);

                }

            }

        return response()->json(['status' => 'OK']);

        }

    }

    public function index()
    {

        return view('administratif::mpaspor.index');
    }

}