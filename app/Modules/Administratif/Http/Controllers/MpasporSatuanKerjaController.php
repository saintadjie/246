<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Auth;

class MpasporSatuanKerjaController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function pullData(Request $request) {

        try {
            
            return datatables ($this->db->select("SELECT a.id, a.nama, CASE WHEN a.non_electronic_availability = 1 THEN '✅' WHEN a.non_electronic_availability = 0 THEN '❌' ELSE 'Undefined' END as non_electronic_availability, CASE WHEN a.electronic_availability = 1 THEN '✅' WHEN a.electronic_availability = 0 THEN '❌' ELSE 'Undefined' END as electronic_availability, CASE WHEN a.polikarbonat_availability = 1 THEN '✅' WHEN a.polikarbonat_availability = 0 THEN '❌' ELSE 'Undefined' END as polikarbonat_availability, CASE WHEN a.show = 1 THEN '✅' WHEN a.show = 0 THEN '❌' ELSE 'Undefined' END as show FROM po_kma.kma_tbl_kanim a ORDER BY a.id"))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function index()
    {

        return view('administratif::mpaspor.satuan_kerja');
    }
}