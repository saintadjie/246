<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class MpasporKuotaTerpakaiController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function pullData(Request $request) {

        $type       = $request->type;

        switch ($type) {
            case 'pullData':
                if ($request->has('satker') && $request->has('tanggal_awal') && $request->has('tanggal_akhir')) {
                    try {
                        $satker         = $request->satker;
                        $satker         = implode(",",$satker);
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');

                        return datatables ($this->db->select("SELECT a.kanim_id, c.nama, a.tanggal_pengajuan, count(a.sesi) as kuota_terpakai FROM po_mpp.mpp_tbl_permohonan a RIGHT JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id JOIN po_kma.kma_tbl_kanim c on a.kanim_id = c.id WHERE c.id in ($satker) AND b.status NOT IN (2,4,1,0) AND a.tanggal_pengajuan >= '$t_awal' AND a.tanggal_pengajuan <= '$t_akhir' GROUP BY a.kanim_id, a.tanggal_pengajuan, c.nama"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } elseif ($request->has('tanggal_awal') && $request->has('tanggal_akhir')) {
                    try {
                        $tanggal_awal   = $request->tanggal_awal;
                        $t_awal         = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_awal. ' 00:00:00');
                        $tanggal_akhir  = $request->tanggal_akhir;
                        $t_akhir        = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal_akhir. ' 23:59:59');

                        return datatables ($this->db->select("SELECT a.kanim_id, c.nama, a.tanggal_pengajuan, count(a.sesi) as kuota_terpakai FROM po_mpp.mpp_tbl_permohonan a RIGHT JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id JOIN po_kma.kma_tbl_kanim c on a.kanim_id = c.id WHERE b.status NOT IN (2,4,1,0) AND a.tanggal_pengajuan >= '$t_awal' AND a.tanggal_pengajuan <= '$t_akhir' GROUP BY a.kanim_id, a.tanggal_pengajuan, c.nama"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }

                } else {
                    try {
            
                        $todayStart     = Carbon::now()->startOfDay();
                        $todayEnd       = Carbon::now()->endOfDay();

                        return datatables ($this->db->select("SELECT a.kanim_id, c.nama, a.tanggal_pengajuan, count(a.sesi) as kuota_terpakai FROM po_mpp.mpp_tbl_permohonan a RIGHT JOIN po_mpp.mpp_tbl_detail_booking_pemohon b ON a.id = b.permohonan_id JOIN po_kma.kma_tbl_kanim c on a.kanim_id = c.id WHERE b.status NOT IN (2,4,1,0) AND a.tanggal_pengajuan >= '$todayStart' AND a.tanggal_pengajuan <= '$todayEnd' GROUP BY a.kanim_id, a.tanggal_pengajuan, c.nama"))
                        ->addIndexColumn()
                        ->toJson();


                    } catch (Exception $e) {
                        report($e);
                        abort(403, 'Unauthorized action.');
                        return false;
                    }
                }
            break;
        }

    }

    public function index()
    {
        $rs         = $this->db->select(DB::raw("SELECT id, nama FROM po_kma.kma_tbl_kanim"));

        return view('administratif::mpaspor.kuota_terpakai', ['rs' => $rs]);
    }
}
