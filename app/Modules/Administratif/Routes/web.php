<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => 'administratif', 'middleware' => 'auth'], function () {

	Route::group(['prefix' => 'user'], function () {

	    Route::GET('/', 'PenggunaController@index')->middleware('permission:Melihat daftar pengguna');
	    Route::GET('add', 'PenggunaController@page_add')->middleware('permission:Menambah data pengguna');
	    Route::GET('pullData', 'PenggunaController@pullData')->name('pullData.pengguna');
        Route::GET('profile', 'PenggunaController@profile')->middleware('permission:Mengubah data pengguna')->name('profile');
        Route::GET('show/{id}', 'PenggunaController@page_show')->middleware('permission:Mengubah data pengguna lain');
        Route::GET('aktif/{id}', 'PenggunaController@aktif')->middleware('permission:Mengembalikan data pengguna terhapus');
        Route::GET('pullDataRoles', 'PenggunaController@pullDataRoles');

	});

	Route::group(['prefix' => 'apapo'], function () {

	    Route::GET('/', 'ApapoController@index')->middleware('permission:Melihat daftar vip');
	    Route::GET('add_kuota', 'ApapoController@page_add')->middleware('permission:Menambah data vip');
	    Route::GET('pullData', 'ApapoController@pullData')->name('pullData.vip');
	    Route::GET('show/{id}', 'ApapoController@page_show')->middleware('permission:Mengubah data vip');

	});

	Route::group(['prefix' => 'lhi'], function () {

	    Route::GET('/', 'LhiController@index')->middleware('permission:Melihat daftar lhi');
	    Route::GET('/index_error', 'LhiController@index_error')->middleware('permission:Melihat daftar lhi');
	    Route::GET('add', 'LhiController@page_add')->middleware('permission:Menambah data lhi');
	    Route::GET('pullData', 'LhiController@pullData')->name('pullData.lhi');
	    Route::GET('pullDataError', 'LhiController@pullData_index_error')->name('pullDataError.lhi');
	    Route::GET('show/{id}', 'LhiController@page_show')->middleware('permission:Mengubah data lhi');
	    Route::GET('show_error/{id}', 'LhiController@page_show_error')->middleware('permission:Mengubah data lhi error');

	});

	Route::group(['prefix' => 'kirka'], function () {

	    Route::GET('/', 'KirkaController@index')->middleware('permission:Melihat daftar kirka');
	    Route::GET('add', 'KirkaController@page_add')->middleware('permission:Menambah data kirka');
	    Route::GET('pullData', 'KirkaController@pullData')->name('pullData.kirka');
	    Route::GET('show/{id}', 'KirkaController@page_show')->middleware('permission:Mengubah data kirka');

	});

	Route::group(['prefix' => 'dbdpripassword'], function () {

	    Route::GET('/', 'DbDpriPasswordController@index')->middleware('permission:Melihat daftar dbdpripassword');
	    Route::GET('pullData', 'DbDpriPasswordController@pullData')->name('pullData.dbdpripassword');

	});

	Route::group(['prefix' => 'mpaspor'], function () {

	    // Route::GET('/get_permohonan', 'MpasporController@get_permohonan')->name('get_permohonan');
	    // Route::GET('/refresh', 'MpasporController@refresh')->name('refresh');
	    // Route::GET('/reset', 'MpasporController@reset')->name('reset');
	    // Route::GET('/check', 'MpasporController@check')->name('check');
	    // Route::GET('/kadaluarsa', 'MpasporController@kadaluarsa')->name('kadaluarsa');
	    // Route::GET('/compile', 'MpasporController@compile')->name('compile');
	    // Route::GET('/check_reschedule', 'MpasporController@check_reschedule')->name('check_reschedule');
	    // Route::GET('simponimpaspor', 'MpasporSimponiController@check')->name('pullData.simponimpaspor');
	    Route::GET('/', 'MpasporController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('/error_log_synx', 'MpasporController@error_log_synx')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('error_log_synx_api', 'MpasporController@error_log_synx_api')->name('pullData.error_log_synx_api');
	    Route::GET('kuota', 'MpasporKuotaController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('pullData', 'MpasporKuotaController@pullData')->name('pullData.mpasporkuota');
	    Route::GET('kuota_terpakai', 'MpasporKuotaTerpakaiController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('pullDataTerpakai', 'MpasporKuotaTerpakaiController@pullData')->name('pullData.mpasporkuotaterpakai');
	    Route::GET('generate', 'MpasporGenerateController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('pullDataGenerate', 'MpasporGenerateController@pullData')->name('pullData.mpasporgenerate');
	    Route::GET('pullDataGenerateYes', 'MpasporGenerateController@pullData')->name('pullData.mpasporgenerateYes');
	    Route::GET('pullDataGenerateNo', 'MpasporGenerateController@pullData')->name('pullData.mpasporgenerateNo');
	    Route::GET('pullDataGenerateKadaluarsa', 'MpasporGenerateController@pullData')->name('pullData.mpasporgeneratekadaluarsa');
	    Route::GET('/status_permohonan', 'MpasporStatusController@status_permohonan')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('/status_permohonan_api', 'MpasporStatusController@status_permohonan_api')->name('pullData.status_permohonan_api');
	    Route::GET('pullDataPermohonan', 'MpasporPermohonanController@pullData')->name('pullData.mpasporpermohonan');
	    Route::GET('permohonan', 'MpasporPermohonanController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('satuan_kerja', 'MpasporSatuanKerjaController@index')->middleware('permission:Melihat daftar mpaspor');
	    Route::GET('pullDataSatuanKerja', 'MpasporSatuanKerjaController@pullData')->name('pullData.mpasporsatuankerja');
	    Route::GET('simponi', 'MpasporSimponiController@index')->middleware('permission:Melihat daftar mpaspor');
	    // Route::GET('/gasssss', 'MpasporController@gasssss')->name('gasssss');
	    
	});

	Route::group(['prefix' => 'dukcapil'], function () {
		// Route::GET('/check', 'DukcapilController@check')->name('check');
	    Route::GET('/', 'DukcapilController@index')->middleware('permission:Melihat data dukcapil');
	    
	});

	Route::group(['prefix' => 'simponi'], function () {
		// Route::GET('/check', 'SimponiController@check')->name('check');
	    Route::GET('/', 'SimponiController@index')->middleware('permission:Melihat data simponi');
	    
	});

	Route::group(['prefix' => 'synxchro'], function () {

	    Route::GET('/', 'SynxchroMpasporController@index')->middleware('permission:Melihat daftar synxchro');
	    Route::GET('/tester', 'MpasporUserStatusController@tester');

	});

	Route::group(['prefix' => 'logs'], function () {
		Route::GET('pullData', 'LogsController@pullData')->name('pullData.logs');
	    Route::GET('/', 'LogsController@index')->middleware('permission:Melihat daftar logs');

	});


});