<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;


class AdministratifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }

}