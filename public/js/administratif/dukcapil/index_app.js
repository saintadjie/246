$(function () {

    $('#btn_check').click(function(){
        if ($('#NIK').val() == '' && $('#NAMA_LGKP').val() == '' && $('#TGL_LHR').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tidak boleh kosong", "error" )
            return
        } else if ($('#NIK').val() == '') {
            Swal.fire( "Kesalahan", "Kolom NIK tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_check,
            data: {
                NIK         : $("#NIK").val(),
                NAMA_LGKP   : $("#NAMA_LGKP").val(),
                TGL_LHR     : $("#TGL_LHR").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $NIK        = data.result.NIK;
                    $NAMA_LGKP  = data.result.NAMA_LGKP;
                    $TGL_LHR    = data.result.TGL_LHR;
                    var html = '<div class="table-responsive"><table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                    '<thead><tr>'+
                    '<th scope="col">NIK</th>'+
                    '<th scope="col">Nama</th>'+
                    '<th scope="col">Tanggal Lahir</th></tr></thead>';
                   
                        html = html + '<td>' + $NIK + '</td>'+
                        '<td>' + $NAMA_LGKP + '</td>'+
                        '<td>' + $TGL_LHR + '</td></tr>';

                    html = html + '</table></div>';
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        html: html,
                        allowOutsideClick : false,
                        title: 'Data ditemukan',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#id_check').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#id_check').val('')
                }
            }
        })
    });

})