$(function () {

    var usernamelama = "{{rs->username}}";

	$('#btn_simpan').click(function(){
        if ($('#username').val() == '') {
            Swal.fire( "Kesalahan", "Kolom NIP tidak boleh kosong", "error" )
            return
        }
        else if ($('#nama').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama tidak boleh kosong", "error" )
            return
        }
        else if ($('#rspermission')[0].selectedIndex < 0) {
            Swal.fire( "Kesalahan", "Permission harus dipilih", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "NIP : "+$('#usernamelama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        usernamelama    : $("#usernamelama").val(),
                        id              : $("#id").val(),
                        username        : $("#username").val(),
                        nama            : $("#nama").val(),
                        alamat          : $("#alamat").val(),
                        status          : $("#status").val(),
                        rspermission    : $("#rspermission").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else if(data.status == 'FAILED' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "NIP sudah ada", "error")
                        } else if(data.status == 'FAILED' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Pengguna dengan NIP: " + $('#username').val() + " sudah ada namun tidak aktif", "error")
                        } else {
                            Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                        }   
                    }
                });
            }
        })

        
    });

    $('#btn_simpan_password').click(function(){
        if ($('#password').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Password tidak boleh kosong", "error" )
            return false
        }
        else if ($('#konfirmasi_password').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Konfirmasi Password tidak boleh kosong", "error" )
            return false
        } 
        else if ($('#password').val() != $('#konfirmasi_password').val()) {
            Swal.fire("Kesalahan", "Password dan Konfirmasi Password tidak cocok", "error");
            return false
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Password : "+$('#usernamelama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_pass,
                    data: {
                        id              : $("#id").val(),
                        password        : $("#password").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                        }   
                    }
                });
            }
        })
        
    });

})