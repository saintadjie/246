$(function () {
    

    $('#btn_simpan').click(function(){
        if ($('#username').val() == '') {
            Swal.fire( "Kesalahan", "Kolom NIP tidak boleh kosong", "error" )
            return false
        }
        else if ($('#nama').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Nama tidak boleh kosong", "error" )
            return false
        }
        else if ($('#password').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Password tidak boleh kosong", "error" )
            return false
        }
        else if ($('#konfirmasi_password').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Konfirmasi Password tidak boleh kosong", "error" )
            return false
        } 
        else if ($('#password').val() != $('#konfirmasi_password').val()) {
            Swal.fire("Kesalahan", "Password dan Konfirmasi Password tidak cocok", "error");
            return false
        }
        else if ($('#rspermission')[0].selectedIndex < 0) {
            Swal.fire( "Kesalahan", "Permission harus dipilih", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                username        : $("#username").val(),
                nama            : $("#nama").val(),
                password        : $("#password").val(),
                alamat          : $("#alamat").val(),
                rspermission    : $("#rspermission").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#username').val('')
                        $('#nama').val('')
                        $('#password').val('')
                        $('#konfirmasi_password').val('')
                        $('#alamat').val('')
                        $('#rspermission').val('').change();
                    })
                } else if(data.status == 'FAILED' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Pengguna sudah ada", "error")
                } else if(data.status == 'FAILED' && data.message == 'Trash') {
                    Swal.fire("Kesalahan", "Pengguna dengan NIP: " + $('#username').val() + " sudah ada namun tidak aktif", "error")
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }
            }
        })
    })

})
