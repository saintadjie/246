$(function () {

	$('#btn_generate_yes').click(function(){
        if ($('#id_permohonan_yes').val() == '') {
            Swal.fire( "Kesalahan", "Kolom ID Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_generate_yes,
            data: {
                id_permohonan_yes    : $("#id_permohonan_yes").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        html: 'Kode Billing berhasil digenerate',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
		                location.href = url_main
                    })
                } else if(data.status == 'ERROR'){

                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        showConfirmButton: true,
                        html: 'Kode Billing gagal digenerate',
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#id_permohonan_yes').tagsinput('removeAll');
                }
            }
        })
    });

	$('#btn_generate_no').click(function(){
        if ($('#id_permohonan_no').val() == '') {
            Swal.fire( "Kesalahan", "Kolom ID Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_generate_no,
            data: {
                id_permohonan_no    : $("#id_permohonan_no").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        html: 'Kode Billing dan Nomor Permohonan berhasil digenerate',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
		                location.href = url_main
                    })
                } else if(data.status == 'ERROR'){

                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        showConfirmButton: true,
                        html: 'Kode Billing dan Nomor Permohonan gagal digenerate',
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#id_permohonan_no').tagsinput('removeAll');
                }
            }
        })
    });

    $('#btn_generate_kadaluarsa').click(function(){

    	Swal.fire({
            title: "Apakah anda yakin?",
            text: "Semua permohonan dengan tanggal pengajuan lebih kecil sama dengan "+date+" akan diubah menjadi kadaluarsa?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
		            type: "POST",
		            url: url_api_kadaluarsa,
		            success: function(data) {
		                if(data.status == 'OK'){
		                    Swal.fire({
		                        icon: 'success',
		                        title: "Data telah diubah",
		                        timer: 3000,
		                        showConfirmButton: true,
		                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
		                        timerProgressBar: true,
		                        onBeforeOpen: () => {
		                            Swal.showLoading()
		                            timerInterval = setInterval(() => {
		                              const content = Swal.getContent()
		                              if (content) {
		                                const b = content.querySelector('b')
		                                if (b) {
		                                  b.textContent = Swal.getTimerLeft()
		                                }
		                              }
		                            }, 100)
		                        },
		                        onClose: () => {
		                            clearInterval(timerInterval)
		                        }
		                    }).then(function (result) {
		                        location.href = url_main
		                    })
		                } else if(data.status=='ERROR'){
		                    Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
		                }
		            }
		        })
            }
        })
        
    })

	var table_form = $('#table_form').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'colvis'
        ],
        "language": {
            "emptyTable":     "Tidak ada data yang tersedia",
            "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
            "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
            "infoFiltered":   "(tersaring dari _MAX_ total data)",
            "lengthMenu":     "Tampilkan _MENU_ data",
            "search":         "Pencarian:",
            "zeroRecords":    "Pencarian tidak ditemukan",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "▶",
                "previous":   "◀"
            },
        },
        "lengthMenu"  : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]],

        destroy: true,
        processing: true,
        serverSide: true,
        order: [[5, 'asc']],
        "ajax": {
            "url": url_all,
            "type": "GET",
            "data": {
                type : 'pullDataGenerate',
            },
        },
        columns: [
            {   
                "data": 'DT_RowIndex',
                "sClass": "text-center",
                "orderable": false, 
                "searchable": false
            },
            {
                "data": "kode_permohonan",
                "sClass": "text-center",
            },
            {
                "data": "nik",
                "sClass": "text-center",
            },
            {
                "data": "nama",
                "sClass": "text-center",
            },
            {
                "data": "tanggal_pengajuan",
                "sClass": "text-center",
            },
            {
                "data": "submit_date",
                "sClass": "text-center",
            },
            {
                "data": "status_detail",
                "sClass": "text-center",
            },
            {
                "data": "status_master",
                "sClass": "text-center",
            },
            {
                "data": "is_delete",
                "sClass": "text-center",
            },
            {
                "data": "id_detail",
                "sClass": "text-center",
            },
        ],
    });

    if ($('#kode_permohonan').on('change', function() { 
        if ($('#kode_permohonan').val() == 'yes') {
            var table_form = $('#table_form').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]],

                destroy: true,
                processing: true,
                serverSide: true,
                order: [[5, 'asc']],
                "ajax": {
                    "url": url_yes,
                    "type": "GET",
                    "data": {
                        type : 'pullDataGenerateYes',
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "kode_permohonan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nik",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_pengajuan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "submit_date",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_detail",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_master",
                        "sClass": "text-center",
                    },
                    {
                        "data": "is_delete",
                        "sClass": "text-center",
                    },
                    {
                        "data": "id_master",
                        "sClass": "text-center",
                    },
                ],
                columnDefs: [
			      	{
				        targets: 9,
				        createdCell: function (td) {
				          	$(td).css('background-color', "LightCyan")
				        }
			      	}
			    ],
            });
            $('#no_value').attr('hidden', true);
            $('#kadaluarsa_value').attr('hidden', true);
            $('#yes_value').removeAttr('hidden');
        } else if ($('#kode_permohonan').val() == 'no'){
            var table_form = $('#table_form').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]],

                destroy: true,
                processing: true,
                serverSide: true,
                order: [[5, 'asc']],
                "ajax": {
                    "url": url_no,
                    "type": "GET",
                    "data": {
                        type : 'pullDataGenerateNo',
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "kode_permohonan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nik",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_pengajuan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "submit_date",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_detail",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_master",
                        "sClass": "text-center",
                    },
                    {
                        "data": "is_delete",
                        "sClass": "text-center",
                    },
                    {
                        "data": "id_detail",
                        "sClass": "text-center",
                    },
                ],
                columnDefs: [
			      	{
				        targets: 9,
				        createdCell: function (td) {
				          	$(td).css('background-color', "LightCyan")
				        }
			      	}
			    ],
            });
            $('#no_value').removeAttr('hidden');
            $('#yes_value').attr('hidden', true);
            $('#kadaluarsa_value').attr('hidden', true);
        } else if ($('#kode_permohonan').val() == 'kadaluarsa'){
            var table_form = $('#table_form').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]],

                destroy: true,
                processing: true,
                serverSide: true,
                order: [[5, 'asc']],
                "ajax": {
                    "url": url_no,
                    "type": "GET",
                    "data": {
                        type : 'pullDataGenerateKadaluarsa',
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "kode_permohonan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nik",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_pengajuan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "submit_date",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_detail",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_master",
                        "sClass": "text-center",
                    },
                    {
                        "data": "is_delete",
                        "sClass": "text-center",
                    },
                    {
                        "data": "id_master",
                        "sClass": "text-center",
                    },
                ],
            });
            $('#kadaluarsa_value').removeAttr('hidden');
            $('#yes_value').attr('hidden', true);
            $('#no_value').attr('hidden', true);
        } else {
            var table_form = $('#table_form').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Semua"]],

                destroy: true,
                processing: true,
                serverSide: true,
                order: [[5, 'asc']],
                "ajax": {
                    "url": url_all,
                    "type": "GET",
                    "data": {
                        type : 'pullDataGenerate',
                    },
                },
                columns: [
                    {   
                        "data": 'DT_RowIndex',
                        "sClass": "text-center",
                        "orderable": false, 
                        "searchable": false
                    },
                    {
                        "data": "kode_permohonan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nik",
                        "sClass": "text-center",
                    },
                    {
                        "data": "nama",
                        "sClass": "text-center",
                    },
                    {
                        "data": "tanggal_pengajuan",
                        "sClass": "text-center",
                    },
                    {
                        "data": "submit_date",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_detail",
                        "sClass": "text-center",
                    },
                    {
                        "data": "status_master",
                        "sClass": "text-center",
                    },
                    {
                        "data": "is_delete",
                        "sClass": "text-center",
                    },
                    {
                        "data": "id_detail",
                        "sClass": "text-center",
                    },
                ],
            });
            $('#no_value').attr('hidden', true);
            $('#yes_value').attr('hidden', true);
            $('#kadaluarsa_value').attr('hidden', true);
        }

    }));

})