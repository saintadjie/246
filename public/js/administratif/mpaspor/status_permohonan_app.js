$(function () {

	$('#btn_update').click(function(){

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Mengubah Semua Status Master menjadi 3?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_api,
                    data: {
                        
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: 'success',
                                width: 600,
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                allowOutsideClick : false,
                                html: 'Status ID Master berhasil diubah',
                                showConfirmButton: true,
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            }).then(function (result) {
                                location.href = url_main
                            })
                        } else if(data.status == 'ERROR'){

                            Swal.fire({
                                icon: 'warning',
                                width: 600,
                                allowOutsideClick : false,
                                showConfirmButton: true,
                                html: 'Status ID Master gagal diubah',
                                showClass: {
                                    popup: 'animate__animated animate__wobble'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutRightBig'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            })
                            
                        }
                    }
                })
            }
            

        })
        
    });


})