$(function () {

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    $('#btn_check').click(function(){
        if ($('#tanggal_pengajuan').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal Permohonan/Kedatangan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_check,
            data: {
                tanggal_pengajuan    : $("#tanggal_pengajuan").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: "success",
                        title: "Permohonan Berhasil dicheck",
                        timer: 3000,
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        showCloseButton: true,
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function(){
                        $('#tanggal_pengajuan').val('')
                    });
                    
                } else {
                    
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: "Permohonan Gagal dicheck",
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#tanggal_pengajuan').val('')
                }
            }
        })
    });

})