$(function () {
    
    $('#btn_reset').click(function(){
        $('#id_tarif').val('')
        $('#daya').val('')
        $('#tarifperkwh').val('')
    });

    $('#btn_simpan').click(function(){
        if ($('#daya').val() == '') {
            Swal.fire( "Kesalahan", "Kolom daya tidak boleh kosong", "error" )
            return
        }
        else if ($('#tarifperkwh').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tarif per KWH tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                id_tarif    : $("#id_tarif").val(),
                daya        : $("#daya").val(),
                tarifperkwh : $("#tarifperkwh").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#id_tarif').val('')
                        $('#daya').val('')
                        $('#tarifperkwh').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Daya sudah ada", "error")
                } else if(data.status == 'Failed' && data.message == 'Trash') {
                    Swal.fire("Kesalahan", "Tarif dengan daya: " + $('#daya').val() + " sudah ada namun tidak aktif", "error")
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                }
            }
        })
    })

})
