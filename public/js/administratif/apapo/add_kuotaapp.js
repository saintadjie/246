$(function () {

    $('#btn_simpan').click(function(){
        if ($('#tanggal').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Tanggal tidak boleh kosong", "error" )
            return
        }
        else if ($('#satker')[0].selectedIndex < 0) {
            Swal.fire( "Kesalahan", "Satuan Kerja harus dipilih", "error" )
            return
        }
        else if ($('#quota').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kuota tidak boleh kosong", "error" )
            return
        }
        else if ($('#availability').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Availability tidak boleh kosong", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Akan membuat Kuota VIP untuk Satuan Kerja "+"?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_api,
                    data: {
                        tanggal         : $("#tanggal").val(),
                        satker          : $("#satker").val(),
                        quota           : $("#quota").val(),
                        availability    : $("#availability").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah disimpan",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                window.location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                allowOutsideClick : false,
                                title: 'Kesalahan',
                                html: 'Data gagal disimpan',
                                showConfirmButton: true,
                                showClass: {
                                    popup: 'animate__animated animate__wobble'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutRightBig'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            })
                        }   
                    }
                });
            }
        })

    })

})
