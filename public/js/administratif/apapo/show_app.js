$(function () {

    $('#tb_imigrasi tbody').on('input', '#quota', function(){

        var quotav              = $(this).closest('tr').find('#quota').val()
        var kuota_terpakaiv     = $(this).closest('tr').find('#kuota_terpakai').val()
        var sisa_kuotan         = $(this).closest('tr').find('#sisa_kuota').attr('name')

        $("input[name=" + sisa_kuotan + "]").val( ((parseInt(quotav, 10) - parseInt(kuota_terpakaiv, 10))))
        
    })

	$('#tb_imigrasi tbody').on('click', '#btn_simpan', function(){

        var id              = $(this).closest('tr').attr('id')
        var quota           = $(this).closest('tr').find('#quota').val()
        var sisa_kuota      = $(this).closest('tr').find('#sisa_kuota').val()
        var sisa_kuotan     = $(this).closest('tr').find('#sisa_kuota').attr('name')

        if ($("input[name=" + sisa_kuotan + "]").val() < 0) {
            Swal.fire( "Kesalahan", "Sisa kuota tidak boleh kurang dari 0", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Jumlah Kuota Percepatan akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_api,
                    data: {
                        id              : id,
                        quota           : quota,
                        availability    : sisa_kuota,
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                allowOutsideClick : false,
                                title: 'Kesalahan',
                                html: 'Data gagal diubah',
                                showConfirmButton: true,
                                showClass: {
                                    popup: 'animate__animated animate__wobble'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutRightBig'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            })
                        }   
                    }
                });
            }
        })
        
    })

    $('#btn_update').click(function(){

        var tanggal_awal    = $('#tanggal_awal').val();
        var tanggal_akhir   = $('#tanggal_akhir').val();

        if ($('#quota').val() == '') {
            Swal.fire( "Kesalahan", "Kolom kuota VIP tidak boleh kosong", "error" )
            return
        }
        else if ($('#availability').val() == '') {
            Swal.fire( "Kesalahan", "Kolom kuota tersedia/terpakai tidak boleh kosong", "error" )
            return
        }
        if ($('#tanggal_awal').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tanggal awal tidak boleh kosong", "error" )
            return
        }
        else if ($('#tanggal_akhir').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tanggal akhir tidak boleh kosong", "error" )
            return
        }
        else if (tanggal_akhir < tanggal_awal){
            Swal.fire( "Kesalahan", "Kolom tanggal akhir tidak boleh lebih besar dari kolom tanggal awal", "error" )
            return
        }



        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Akan menambah Kuota VIP dari tanggal "+$('#tanggal_awal').val()+" hingga tanggal "+$('#tanggal_akhir').val()+"?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api_update,
                    data: {
                        id              : $("#satker").val(),
                        quota           : $("#quota").val(),
                        availability    : $("#availability").val(),
                        tanggal_awal    : $("#tanggal_awal").val(),
                        tanggal_akhir   : $("#tanggal_akhir").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah disimpan",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                window.location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                allowOutsideClick : false,
                                title: 'Kesalahan',
                                html: 'Data gagal disimpan',
                                showConfirmButton: true,
                                showClass: {
                                    popup: 'animate__animated animate__wobble'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutRightBig'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            })
                        }   
                    }
                });
            }
        })
        
    })

})