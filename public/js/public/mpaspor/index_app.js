$(function () {

    $('#btn_kpnkb').click(function(){
        if ($('#kpnkb').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_kpnkb,
            data: {
                kpnkb    : $("#kpnkb").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {

                $nomor = 0;
                if(data.status == 'OK'){
                    var html = '<div class="table-responsive"><table id="tb_imigrasi" class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                    '<thead bgcolor="DarkSlateBlue"><tr>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">No.</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kode Permohonan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">NIK</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Nama</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Satuan Kerja</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Status Detail</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Status Permohonan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kode Billing</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kadaluarsa Kode Billing</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Tanggal Submit</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Tanggal Kedatangan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Sesi</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Rescheduled?</th></tr></thead><tbody>';
                    for(var i=0 ; i<data.rs.length ; i++) {
                        $nomor++;
                        html = html + '<tr id="' + data.rs[i].id + '"><td>' + $nomor + '</td>'+
                        '<td>' + data.rs[i].kode_permohonan + '</td>'+
                        '<td>' + data.rs[i].nik + '</td>'+
                        '<td>' + data.rs[i].nama + '</td>'+
                        '<td>' + data.rs[i].satker + '</td>'+
                        '<td>' + data.rs[i].status_detail + '</td>'+
                        '<td>' + data.rs[i].status_permohonan + '</td>'+
                        '<td>' + data.rs[i].simponi_billing_code + '</td>'+
                        '<td>' + data.rs[i].kadalauarsa_kode_billing + '</td>'+
                        '<td>' + data.rs[i].tanggal_submit + '</td>'+
                        '<td>' + data.rs[i].tanggal_kedatangan + '</td>'+
                        '<td>' + data.rs[i].nama_sesi + '</td>'+
                        '<td>' + data.rs[i].rescheduled + '</td>' + '</td></tr>';
                    }
                    html = html + '</tbody></table></div>';
                    Swal.fire({
                        title: "Data ditemukan!",
                        // imageUrl: ("/images/komang.jpeg"),
                        // imageWidth: 100,
                        // imageHeight: 100,
                        // imageAlt: 'Sukses!',
                        icon: 'success',
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        customClass: 'swal-wide',
                        allowOutsideClick : false,
                        html: html,
                        showConfirmButton: true,
                        showCloseButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kpnkb').val('')
                    })
                } else {
                    Swal.fire({
                        title: "Kesalahan",
                        html: "Kode Permohonan, NIK atau Kode Billing tidak ditemukan!",
                        icon: 'error',
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                    })
                    $('#kpnkb').val('')
                }
            }
        })
    });
    

    $('#btn_kode_billing').click(function(){
        if ($('#kode_billing').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Billing tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_simponi,
            data: {
                kode_billing         : $("#kode_billing").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $kode_billing        = data.result.data[3];
                    var html = '<div class="table-responsive"><table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                    '<thead><tr>'+
                    '<th scope="col">Kode Billing</th>'+
                    '<th scope="col">NTB</th>'+
                    '<th scope="col">NTPN</th>'+
                    '<th scope="col">Tgl Jam Pembayaran</th>'+
                    '<th scope="col">Tgl Buku</th>'+
                    '<th scope="col">Bank Persepsi</th>'+
                    '<th scope="col">Channel Pembayaran</th>'+
                    '<th scope="col">Nama di M-Paspor</th></tr></thead>';
                   
                        html = html + '<td>' + $kode_billing + '</td>'+
                        '<td>' + data.result.response.data[1] + '</td>'+
                        '<td>' + data.result.response.data[2] + '</td>'+
                        '<td>' + data.result.response.data[3] + '</td>'+
                        '<td>' + data.result.response.data[6] + '</td>'+
                        '<td>' + data.result.response.data[4] + '</td>'+
                        '<td>' + data.result.response.data[5] + '</td>'+
                        '<td>' + data.datampaspor + '</td></tr>';

                    html = html + '</table></div>';
                    Swal.fire({
                        icon: 'success',
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        customClass: 'swal-wide',
                        html: html,
                        allowOutsideClick : false,
                        title: 'Data ditemukan',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_billing').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_billing').val('')
                }
            }
        })
    });

    $('#btn_reschedule').click(function(){
        if ($('#kode_permohonan_reschedule').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_reschedule,
            data: {
                kode_permohonan_reschedule    : $("#kode_permohonan_reschedule").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 800,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        title: 'Kode Permohonan ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        html : '<div class="table-responsive">' + 
                        '<table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                        '<tbody><tr>'+
                        '<th scope="row">NIK</th>' + 
                        '<td>'+ data.rs[0].nik + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Nama</th>' + 
                        '<td>'+ data.rs[0].nama + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Kode Permohonan</th>' + 
                        '<td>'+ data.rs[0].kode_permohonan + '</td>' +
                        '</tr><tr>' +
                        '<th scope="row">Jadwal Awal</th>' + 
                        '<td>'+ data.rs[0].kanim_sebelum + ', ' + data.rs[0].tanggal_sebelum + '(' + data.rs[0].sesi_sebelum + ')' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Jadwal Reschedule</th>' + 
                        '<td>'+ data.rs[0].kanim_sesudah + ', ' + data.rs[0].tanggal_sesudah + '(' + data.rs[0].sesi_sesudah + ')' + '</td>' +
                        '</tr>' + 
                        '</tbody></table></div>',

                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_permohonan_reschedule').val('')
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: 'Kode Permohonan tidak ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_reschedule').val('')
                }
            }
        })
    });

    $('#btn_status_email').click(function(){
        if ($('#status_email').val() == '') {
            Swal.fire( "Kesalahan", "Kolom email tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_status_email,
            data: {
                status_email    : $("#status_email").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 800,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : true,
                        title: 'Email Ditemukan',
                        showCloseButton: true,
                        html : '<div class="table-responsive">' + 
                        '<table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                        '<tbody><tr>'+
                        '<th scope="row">Email</th>' + 
                        '<td>'+ '<input type="text" id="email_email" class="form-control" disabled value="' + data.rs[0].email + '">' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Nomor Telepon</th>' + 
                        '<td>'+ '<input type="text" id="email_nomor_telepon" class="form-control" disabled value="' + data.rs[0].nomor_telepon + '">' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Status</th>' + 
                        '<td>'+ '<input type="text" id="email_status" class="form-control" disabled value="' + data.rs[0].status + '">' + '</td>' +
                        '</tr>' + 
                        '</tbody></table></div>',

                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#status_email').val('')
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: 'Email tidak ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#status_email').val('')
                }
            }
        })
    });

})