<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (!session('username')) {
		return redirect ('login');
	} else {
		return view('home');
	}
});

Auth::routes([
  'register' => true, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mpaspor', 'PublicMpasporController@index')->name('PublicMpasporIndex');
Route::get('/mpaspor/kuota', 'PublicMpasporController@kuota')->name('PublicMpasporKuota');
Route::GET('/mpaspor/pullData', 'PublicMpasporController@pullData')->name('pullData.publicmpasporkuota');

Route::get('/cek', 'HomeController@cek')->name('cek');

Route::get('errors', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);