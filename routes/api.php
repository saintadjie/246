<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1/public'], function () {

    Route::group(['prefix' => 'mpaspor'], function () {
        Route::POST('get_permohonanPm', 'PublicMpasporController@get_permohonanPm');
        Route::POST('check_simponiPm', 'PublicMpasporController@check_simponiPm');
        Route::POST('check_reschedulePm', 'PublicMpasporController@check_reschedulePm');
        Route::POST('status_emailPm', 'PublicMpasporController@status_emailPm');
    });


});